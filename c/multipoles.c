#include "extensions.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

void arrange(int s[5], const int rank){
  /* Given a sequence of ints and a rank 
     arrange in ascending order
  */
  for (int i = 0; i < rank; i++){
    for (int j = 0; j < rank; j++){
      if (s[j] > s[i]){
        int tmp = s[i];
        s[i] = s[j];
        s[j] = tmp;
      }
    }
  }
}

void mic(double r[3], const double pbc[3], const double Rc_v[3]){
  /* Translate vector by cell dimension if pbc
  */
  for (int v = 0; v < 3; v++){
    if (pbc[v] > 0){
      double s = round(r[v] / Rc_v[v]) * Rc_v[v];
      r[v] -= s;
    }
  }
}

void nn_matrix(const long* Nc_v, double r_m[][3], 
               double r_o[3], const double Rc_v[3],
               int noc){
  /* Make nearest neighbor matrix of distances
  */
  for (int l = 0; l < noc; l++){
    const long* Nc = Nc_v + l * 3;
    r_m[l][0] = r_o[0] + Nc[0] * Rc_v[0];
    r_m[l][1] = r_o[1] + Nc[1] * Rc_v[1];
    r_m[l][2] = r_o[2] + Nc[2] * Rc_v[2];

  }
}

PyObject *calculate_moments(PyObject *self, PyObject *args)
{
  PyArrayObject* beg_v_obj;
  PyArrayObject* h_v_obj;
  PyArrayObject* R_v_obj;     // Center in Bohr : default (0,0,0)
  PyArrayObject* rhot_G_obj;
  PyArrayObject* D_a_obj;
  PyArrayObject* Q_ab_obj;
  PyArrayObject* O_abc_obj;
  PyArrayObject* H_abcd_obj;
  PyArrayObject* Rc_v_obj;
  PyArrayObject* pbc_v_obj;

  if (!PyArg_ParseTuple(args, "OOOOOOOOOO", &beg_v_obj, &h_v_obj, &R_v_obj,
                        &rhot_G_obj, &D_a_obj, &Q_ab_obj, &O_abc_obj, &H_abcd_obj,
                        &Rc_v_obj, &pbc_v_obj))
  return NULL;

  const long *beg_v = PyArray_DATA(beg_v_obj);
  const double *h_v = PyArray_DATA(h_v_obj);
  const double *R_v = PyArray_DATA(R_v_obj);
  const double *rhot_G = PyArray_DATA(rhot_G_obj);
  double *D_a = PyArray_DATA(D_a_obj);
  double *Q_ab = PyArray_DATA(Q_ab_obj);
  double *O_abc = PyArray_DATA(O_abc_obj);
  double *H_abcd = PyArray_DATA(H_abcd_obj);
  const double *Rc_v = PyArray_DATA(Rc_v_obj);
  const double *pbc_v = PyArray_DATA(pbc_v_obj);

  npy_intp* n = PyArray_DIMS(rhot_G_obj);

  const double dV = h_v[0] * h_v[1] * h_v[2];
  double r[3];
  double rr;
  double rq;
  double ro;
  double rh;
  double rrh;
  double rt;
  
  int nn = n[0];

  // Loop over grid
  for (int i = 0; i < n[0]; i++){
    double x = (beg_v[0] + i) * h_v[0];
    for (int j = 0; j < n[1]; j++){
      double y = (beg_v[1] + j) * h_v[1];
      int ij = (i * n[1] + j) * n[2];
      for (int k = 0; k < n[2]; k++){
        double z = (beg_v[2] + k) * h_v[2];
        int G = ij + k;

        r[0] = x - R_v[0]; 
        r[1] = y - R_v[1]; 
        r[2] = z - R_v[2]; 

        rr = (r[0] * r[0] + r[1] * r[1] + r[2] * r[2]);
        rq = rr / 3.0;
        ro = rr / 5.0;
        rh = rr / 7.0;
        rrh = ro * rh;
        // rt = rr / 9.0;

        double cd = rhot_G[G] * dV;
        double cq = cd * 3.0 / 2.0;
        double co = cd * 15.0 / 6.0;
        double cH = cd * 105.0 / 24.0;
        // double cT = cd * 9.0;

        // Dipoles
        D_a[0] += cd * r[0];
        D_a[1] += cd * r[1];
        D_a[2] += cd * r[2];

        // Get the 6 unique quadrupole elements
        Q_ab[0] += cq * (r[0] * r[0] - rq);
        Q_ab[1] += cq * r[0] * r[1];
        Q_ab[2] += cq * r[0] * r[2];
        Q_ab[4] += cq * (r[1] * r[1] - rq);
        Q_ab[5] += cq * r[1] * r[2];
        Q_ab[8] += cq * (r[2] * r[2] - rq);

        // Get the 10 unique octupole components
        O_abc[0] += co * (r[0] * r[0] * r[0] - 3.0 * r[0] * ro);
        O_abc[1] += co * (r[0] * r[0] * r[1] - r[1] * ro);
        O_abc[2] += co * (r[0] * r[0] * r[2] - r[2] * ro);
        O_abc[4] += co * (r[0] * r[1] * r[1] - r[0] * ro);
        O_abc[5] += co * r[0] * r[1] * r[2];
        O_abc[8] += co * (r[0] * r[2] * r[2] - r[0] * ro);
        O_abc[13] += co * (r[1] * r[1] * r[1] - 3.0 * r[1] * ro);
        O_abc[14] += co * (r[1] * r[1] * r[2] - r[2] * ro);
        O_abc[17] += co * (r[1] * r[2] * r[2] - r[1] * ro);
        O_abc[26] += co * (r[2] * r[2] * r[2] - 3.0 * r[2] * ro);

        // Get the 15 unique hexadecapole components
        H_abcd[0] += cH * (r[0] * r[0] * r[0] * r[0] - 6.0 * rh * r[0] * r[0] + 3.0 * rrh);
        H_abcd[1] += cH * (r[0] * r[0] * r[0] * r[1] - 3.0 * rh * r[0] * r[1]);
        H_abcd[2] += cH * (r[0] * r[0] * r[0] * r[2] - 3.0 * rh * r[0] * r[2]);
        H_abcd[4] += cH * (r[0] * r[0] * r[1] * r[1] - rh * (r[0] * r[0] + r[1] * r[1]) + rrh);
        H_abcd[5] += cH * (r[0] * r[0] * r[1] * r[2] - rh * r[1] * r[2]);
        H_abcd[8] += cH * (r[0] * r[0] * r[2] * r[2] - rh * (r[0] * r[0] + r[2] * r[2]) + rrh);
        H_abcd[13] += cH * (r[0] * r[1] * r[1] * r[1] - 3.0 * rh * r[0] * r[1]);
        H_abcd[14] += cH * (r[0] * r[1] * r[1] * r[2] - rh * r[0] * r[2]);
        H_abcd[17] += cH * (r[0] * r[1] * r[2] * r[2] - rh * r[0] * r[1]);
        H_abcd[26] += cH * (r[0] * r[2] * r[2] * r[2] - 3.0 * rh * r[0] * r[2]);
        H_abcd[40] += cH * (r[1] * r[1] * r[1] * r[1] - 6.0 * rh * r[1] * r[1] + 3.0 * rrh);
        H_abcd[41] += cH * (r[1] * r[1] * r[1] * r[2] - 3.0 * rh * r[1] * r[2]);
        H_abcd[44] += cH * (r[1] * r[1] * r[2] * r[2] - rh * (r[1] * r[1] + r[2] * r[2]) + rrh);
        H_abcd[53] += cH * (r[1] * r[2] * r[2] * r[2] - 3.0 * rh * r[1] * r[2]);
        H_abcd[80] += cH * (r[2] * r[2] * r[2] * r[2] - 6.0 * rh * r[2] * r[2] + 3.0 * rrh);

        //// Get the 21 unique 32-pole componenents
        //T_abcde[0] += cT * (r[0] * r[0] * r[0] * r[0] * r[0])
        //T_abcde[1] += cT * (r[0] * r[0] * r[0] * r[0] * r[1])
        //T_abcde[2] += cT * (r[0] * r[0] * r[0] * r[0] * r[2])
        //T_abcde[4] += cT * (r[0] * r[0] * r[0] * r[1] * r[1])
        //T_abcde[5] += cT * (r[0] * r[0] * r[0] * r[1] * r[2])
        //T_abcde[8] += cT * (r[0] * r[0] * r[0] * r[2] * r[2])
        //T_abcde[13] += cT * (r[0] * r[0] * r[1] * r[1]* r[1])
        //T_abcde[14] += cT * (r[0] * r[0] * r[1] * r[1] * r[2])
        //T_abcde[17] += cT * (r[0] * r[0] * r[1] * r[2] * r[2])
        //T_abcde[26] += cT * (r[0] * r[0] * r[2] * r[2] * r[2])
        //T_abcde[40] += cT * (r[0] * r[1] * r[1] * r[1] * r[1])
        //T_abcde[41] += cT * (r[0] * r[1] * r[1] * r[1] * r[2])
        //T_abcde[44] += cT * (r[0] * r[1] * r[1] * r[2] * r[2]) 
        //T_abcde[53] += cT * (r[0] * r[1] * r[2] * r[2] * r[2])
        //T_abcde[80] += cT * (r[0] * r[2] * r[2] * r[2] * r[2])
        //T_abcde[121] += cT * (r[1] * r[1] * r[1] * r[1] * r[1])
        //T_abcde[122] += cT * (r[1] * r[1] * r[1] * r[1] * r[2])
        //T_abcde[125] += cT * (r[1] * r[1] * r[1] * r[2] * r[2])
        //T_abcde[134] += cT * (r[1] * r[1] * r[2] * r[2] * r[2])
        //T_abcde[161] += cT * (r[1] * r[2] * r[2] * r[2] * r[2])
        //T_abcde[243] += cT * (r[2] * r[2] * r[2] * r[2] * r[2])
      }
    }
  }
  Py_RETURN_NONE;
}

PyObject *multipole_potential(PyObject *self, PyObject *args)
{
  PyArrayObject* beg_v_obj;
  PyArrayObject* h_v_obj;
  PyArrayObject* d_pv_obj;
  PyArrayObject* q_pv_obj;
  PyArrayObject* R_pv_obj;
  PyArrayObject* dcom_pv_obj;
  PyArrayObject* vext_G_obj;
  PyArrayObject* Nc_v_obj;
  double lrc;
  double width;
  double damp_f;
  PyArrayObject* pbc_v_obj;
  PyArrayObject* Rc_v_obj;
  PyArrayObject* vextoh_G_obj=0;
  PyArrayObject* o_pv_obj = 0;
  PyArrayObject* t_pv_obj = 0;
  PyArrayObject* oc_v_obj = 0;
  PyArrayObject* tc_v_obj = 0;
  if (!PyArg_ParseTuple(args, "OOOOOOOOdddOO|OOOOO", &beg_v_obj, &h_v_obj, &d_pv_obj,
                        &q_pv_obj, &R_pv_obj, &dcom_pv_obj, &vext_G_obj, &Nc_v_obj, &lrc, &width, 
                        &damp_f, &pbc_v_obj, &Rc_v_obj, &vextoh_G_obj, &o_pv_obj, 
                        &t_pv_obj, &oc_v_obj, &tc_v_obj))
  return NULL;

  const long *beg_v = PyArray_DATA(beg_v_obj);
  const double *h_v = PyArray_DATA(h_v_obj);
  const double *d_pv = PyArray_DATA(d_pv_obj);
  const double *q_pv = PyArray_DATA(q_pv_obj);
  const double *R_pv = PyArray_DATA(R_pv_obj);
  const double *dcom_pv = PyArray_DATA(dcom_pv_obj);
  double *vext_G = PyArray_DATA(vext_G_obj);
  const long *Nc_v = PyArray_DATA(Nc_v_obj);
  const double *pbc_v = PyArray_DATA(pbc_v_obj);
  const double *Rc_v = PyArray_DATA(Rc_v_obj);

  int np = PyArray_DIM(R_pv_obj, 0);
  npy_intp* n = PyArray_DIMS(vext_G_obj);
  int noc = PyArray_DIM(Nc_v_obj, 0);
  int vec = PyArray_DIM(Nc_v_obj, 1);

  // Octupole-Hexadecapole (OH) contribution to the external potential
  double* vextoh_G = 0;
  const double* o_pv = 0;
  const double* t_pv = 0;
  const double* oc_v = 0;
  const double* tc_v = 0;
  if (vextoh_G_obj != 0) {
    vextoh_G = PyArray_DATA(vextoh_G_obj);
    o_pv = PyArray_DATA(o_pv_obj);
    t_pv = PyArray_DATA(t_pv_obj);
    oc_v = PyArray_DATA(oc_v_obj);
    tc_v = PyArray_DATA(tc_v_obj);
  }

  const double lrc12 = lrc - width;
  double r[3];
  double r_o[3]; // distance vector in (0,0,0)
  // distance vectors btw (0,0,0) and all neighbour cells
  double r_m[noc][3];

  for (int i = 0; i < n[0]; i++){
    double x = (beg_v[0] + i) * h_v[0];
    for (int j = 0; j < n[1]; j++){
      double y = (beg_v[1] + j) * h_v[1];
      int ij = (i * n[1] + j) * n[2];
      for (int k = 0; k < n[2]; k++){
        double z = (beg_v[2] + k) * h_v[2];
        int G = ij + k;
        for (int p = 0; p < np; p++){
          // Long-range cut-off      
          const double* dcom_v = dcom_pv + 3 * p;
          double dc, dxc, dyc, dzc;  
          // PBC logics              
          dxc = (1 - pbc_v[0]) * dcom_v[0]; // dcom_v[0] + pbc_v[0] * (r[0] - dcom_v[0]);
          dyc = (1 - pbc_v[1]) * dcom_v[1];
          dzc = (1 - pbc_v[2]) * dcom_v[2];
          dc = sqrt(dxc * dxc + dyc * dyc + dzc * dzc);
          // Check long range
          double fsw = 1.0; 
          if (dc > lrc) // Outside of lrc; skip p
            goto cnt; // fsw *= 0.0;
          else if (dc > lrc12){ 
            double xx = (dc - lrc12) / width;
            fsw -= xx * xx * (3.0 - 2.0 * xx);
          }

          const double* R_v = R_pv + 3 * p;
          const double* d_v = d_pv + 3 * p;
          const double* q_v = q_pv + 9 * p;
          r_o[0] = x - R_v[0];
          r_o[1] = y - R_v[1];
          r_o[2] = z - R_v[2];
          // 
          nn_matrix(Nc_v, r_m, r_o, Rc_v, noc);

          for (int w = 0; w < noc; w++){
            r[0] = r_m[w][0];
            r[1] = r_m[w][1];
            r[2] = r_m[w][2];

            double d = sqrt(r[0] * r[0] + r[1] * r[1] + r[2] * r[2]);
            double d2 = d * d;

            // Calculate interaction tensors and apply fsw
            double dot = 1.0 / (d2 * d) * fsw;
            double dof = dot / d2;
            // Calculate dipole and quadrupole contrb. to potential:
            double dp = d_v[0] * r[0] + d_v[1] * r[1] + d_v[2] * r[2];
            double qp = q_v[0] * r[0] * r[0] + q_v[4] * r[1] * r[1] + q_v[8] * r[2] * r[2]
                        + 2.0 * q_v[1] * r[0] * r[1] + 2.0 * q_v[2] * r[0] * r[2]
                        + 2.0 * q_v[5] * r[1] * r[2];
            double qp_d = 1.0 / 3.0 * (q_v[0] + q_v[4] + q_v[8]);
            // Damping functions:
            double S = d / damp_f;
            double S2 = S * S;
            double S3 = S2 * S;
            double fac;
            double damp_d;
            double damp_q;

            fac = 2.0 / sqrt(M_PI) * exp(-S2);
            damp_d = erf(S) - S * fac;
            damp_q = damp_d - 2.0 / 3.0 * S3 * fac;

            // Sum up contributions:
            vext_G[G] += (dp - qp_d) * damp_d * dot + qp * damp_q * dof;
            // Octup-Hexadecap contrb. to potential
            if (vextoh_G != 0){
              const double* o_v = o_pv + 27 * p;
              const double* t_v = t_pv + 81 * p;
              double dos = dof / d2;
              double don = dos / d2;
              double damp_o;
              double damp_t;

              damp_o = damp_q - 4.0 / 15.0
                       * S3 * S2 * fac;
              damp_t = damp_o - 8.0 / 105.0
                       * S3 * S2 * S2 * fac;
              int oc = 0;
              for (int a = 0; a < 3; a++){
                int aaa = a * 13;
                int aaaa = aaa + a * 27;

                vextoh_G[G] -= 1.0 / 5.0 * o_v[aaa] * damp_q * dof * oc_v[oc] * r[a];
                vextoh_G[G] -= 1.0 / 7.0 * t_v[aaaa] * damp_o * dos * tc_v[aaaa] * r[a] * r[a];
                vextoh_G[G] += 2.0 / 35.0 * t_v[aaaa] * damp_q * dof * tc_v[aaaa];

                for (int b = a; b < 3; b++){
                  int aab = a * 12 + b;
                  int aaab = aab + a * 27;
                  int abb = a * 9 + b * 4;
                  int abbb = a * 27 + b * 13;
                  int aabb = abb + a * 27;
                  
                  vextoh_G[G] -= 1.0 / 5.0 * o_v[aab] * damp_q * dof * oc_v[oc] * r[b];
                  vextoh_G[G] -= 1.0 / 5.0 * o_v[abb] * damp_q * dof * oc_v[oc] * r[a];
                  vextoh_G[G] -= 1.0 / 7.0 * t_v[aaab] * damp_o * dos * tc_v[aaab] * r[a] * r[b];
                  vextoh_G[G] -= 1.0 / 7.0 * t_v[abbb] * damp_o * dos * tc_v[abbb] * r[a] * r[b];
                  vextoh_G[G] += 1.0 / 35.0 * t_v[aabb] * damp_q * dof * tc_v[aabb];

                  for (int c = b; c < 3; c++){
                    int aabc = a * 36 + b * 3 + c;
                    int abbc = a * 27 + b * 12 + c;
                    int abcc = a * 27 + b * 9 + c * 4;

                    vextoh_G[G] -= 1.0 / 7.0 * t_v[aabc] * damp_o * dos * tc_v[aabc] * r[b] * r[c];
                    vextoh_G[G] -= 1.0 / 7.0 * t_v[abbc] * damp_o * dos * tc_v[abbc] * r[a] * r[c];
                    vextoh_G[G] -= 1.0 / 7.0 * t_v[abcc] * damp_o * dos * tc_v[abcc] * r[a] * r[b];

                    int abc = a * 9 + b * 3 + c;

                    vextoh_G[G] += o_v[abc] * r[a] * r[b] * r[c]
                                   * damp_o * dos * oc_v[oc];
                    oc += 1;
                    for (int f = c; f < 3; f++){
                      int abcf = a * 27 + b * 9 + c * 3 + f;

                      vextoh_G[G] += t_v[abcf] * r[a] * r[b] * r[c] * r[f]
                                     * damp_t * don * tc_v[abcf];
                    }
                  } 
                }
              }
            }
          }
        cnt:;
        }
      }
    }
  }
  Py_RETURN_NONE;
}


// Placeholder for the interaction between a multipole expanded QM system and MM sites
PyObject *electric_field_expansion(PyObject *self, PyObject *args)
{  
  PyArrayObject* R_pv_obj; // Position of the MMs
  PyArrayObject* QC_v_obj; // Center for the QM moments
  PyArrayObject* Rc_v_obj; // {Rx,Ry,Rz} of the cell
  PyArrayObject* Nc_v_obj; // list of ints for translation operator
  PyArrayObject* e_pv_obj; // e-field at MM sites
  PyArrayObject* de_pv_obj; // de-field at MM sites
  // Skip long-range cut-off for now, and no reason to evaluate damping due to the long range
  PyArrayObject* d_v_obj; // Multipole moments of QM density
  PyArrayObject* q_v_obj; 
  PyArrayObject* o_v_obj;
  PyArrayObject* h_v_obj;
  PyArrayObject* qc_v_obj; // Multipliers for moments
  PyArrayObject* oc_v_obj;
  PyArrayObject* hc_v_obj;
  PyArrayObject* pbc_v_obj; // ints representing PBC
  // Higher order gradients of the electrostatic interactions:
  PyArrayObject* d2e_pv_obj = 0;
  PyArrayObject* d3e_pv_obj = 0;
  PyArrayObject* d4e_pv_obj = 0;

  if (!PyArg_ParseTuple(args, "OOOOOOOOOOOOO|OOO", &R_pv_obj, &QC_v_obj, &Rc_v_obj, &Nc_v_obj,
                        &e_pv_obj, &de_pv_obj, &d_v_obj, &q_v_obj, &o_v_obj, &h_v_obj,
                        &qc_v_obj, &oc_v_obj, &hc_v_obj, &pbc_v_obj, d2e_pv_obj,
                        &d3e_pv_obj, &d4e_pv_obj))
  return NULL;

  // Terms
  const double *R_pv = PyArray_DATA(R_pv_obj);
  const double *QC_v = PyArray_DATA(QC_v_obj);
  const double *Rc_v = PyArray_DATA(Rc_v_obj);
  const long *Nc_v = PyArray_DATA(Nc_v_obj);
  double *e_pv = PyArray_DATA(e_pv_obj);
  double *de_pv = PyArray_DATA(de_pv_obj);
  const double *d_v = PyArray_DATA(d_v_obj);
  const double *q_v = PyArray_DATA(q_v_obj);
  const double *o_v = PyArray_DATA(o_v_obj);
  const double *h_v = PyArray_DATA(h_v_obj);
  const double *qc_v = PyArray_DATA(qc_v_obj);
  const double *oc_v = PyArray_DATA(oc_v_obj);
  const double *hc_v = PyArray_DATA(hc_v_obj);
  const double *pbc_v = PyArray_DATA(pbc_v_obj);

  // Dimensions
  int np  = PyArray_DIM(R_pv_obj, 0);
  int noc = PyArray_DIM(Nc_v_obj, 0);

  // Distances
  double r[3];
  double r_o[3]; // origin
  double r_m[noc][3]; // images

  // Cell center
  double Rc[3]; Rc[0] = QC_v[0]; Rc[1] = QC_v[1]; Rc[2] = QC_v[2];

  // Handle 3rd-5th order gradient of the potential
  double* d2e_pv = 0;
  double* d3e_pv = 0;
  double* d4e_pv = 0;
  if (d2e_pv_obj != 0){
    d2e_pv = PyArray_DATA(d2e_pv_obj);
    d3e_pv = PyArray_DATA(d3e_pv_obj);
    d4e_pv = PyArray_DATA(d4e_pv_obj);
  }

  // We loop over MM molecules (p)
  for (int p = 0; p < np; p++){
    const double* R_v = R_pv + 3 * p;
    double* e_v = e_pv + 3 * p;
    double* de_v = de_pv + 9 * p;
    double* d2e_v = d2e_pv + 27 * p;
    double* d3e_v = d3e_pv + 81 * p;
    double* d4e_v = d4e_pv + 243 * p;

    r_o[0] = R_v[0] - Rc[0]; // - R_v[0]; //
    r_o[1] = R_v[1] - Rc[1]; // - R_v[1]; //
    r_o[2] = R_v[2] - Rc[2]; // - R_v[2]; //

    nn_matrix(Nc_v, r_m, r_o, Rc_v, noc);

    // Loop over images
    for (int w = 0; w < noc; w++){
      r[0] = r_m[w][0];
      r[1] = r_m[w][1];
      r[2] = r_m[w][2];
      // 
      double d = sqrt(r[0] * r[0] + r[1] * r[1] + r[2] * r[2]);
      double d2 = d * d;
      double d3 = d2 * d;
      double d5 = d3 * d2;
      double d7 = d5 * d2;
      double d9 = d7 * d2;
      double d11 = d9 * d2;
      double d13 = d11 * d2;

      double r00 = r[0] * r[0];
      double r01 = r[0] * r[1];
      double r02 = r[0] * r[2];
      double r11 = r[1] * r[1];
      double r12 = r[1] * r[2];
      double r22 = r[2] * r[2];

      // rank-0 contraction
      double dp = d_v[0] * r[0] + 
                  d_v[1] * r[1] + 
                  d_v[2] * r[2];

      double qp = qc_v[0] * q_v[0] * r00 + 
                  qc_v[1] * q_v[1] * r01 +
                  qc_v[2] * q_v[2] * r02 +
                  qc_v[4] * q_v[4] * r11 +
                  qc_v[5] * q_v[5] * r12 +
                  qc_v[8] * q_v[8] * r22;

      double op = oc_v[0]  * o_v[0]  * r[0] * r00 +
                  oc_v[1]  * o_v[1]  * r[0] * r01 +
                  oc_v[2]  * o_v[2]  * r[0] * r02 +
                  oc_v[4]  * o_v[4]  * r[0] * r11 +
                  oc_v[5]  * o_v[5]  * r[0] * r12 +
                  oc_v[8]  * o_v[8]  * r[0] * r22 +
                  oc_v[13] * o_v[13] * r[1] * r11 +
                  oc_v[14] * o_v[14] * r[1] * r12 +
                  oc_v[17] * o_v[17] * r[1] * r22 +
                  oc_v[26] * o_v[26] * r[2] * r22;

      double hp = hc_v[0]  * h_v[0]  * r00 * r00 +
                  hc_v[1]  * h_v[1]  * r00 * r01 +
                  hc_v[2]  * h_v[2]  * r00 * r02 +
                  hc_v[4]  * h_v[4]  * r00 * r11 +
                  hc_v[5]  * h_v[5]  * r00 * r12 +
                  hc_v[8]  * h_v[8]  * r00 * r22 + 
                  hc_v[13] * h_v[13] * r01 * r11 +
                  hc_v[14] * h_v[14] * r01 * r12 +
                  hc_v[17] * h_v[17] * r01 * r22 +
                  hc_v[26] * h_v[26] * r02 * r22 +
                  hc_v[40] * h_v[40] * r11 * r11 +
                  hc_v[41] * h_v[41] * r11 * r12 +
                  hc_v[44] * h_v[44] * r11 * r22 +
                  hc_v[53] * h_v[53] * r12 * r22 +
                  hc_v[80] * h_v[80] * r22 * r22;

      // rank-1 contraction
      double q2p[3];
      double o2p[3];
      double h2p[3];

      // quadrupole
      q2p[0] = q_v[0] * r[0] +
               q_v[1] * r[1] +
               q_v[2] * r[2]; 

      q2p[1] = q_v[1] * r[0] + 
               q_v[4] * r[1] + 
               q_v[5] * r[2];  

      q2p[2] = q_v[2] * r[0] + 
               q_v[5] * r[1] + 
               q_v[8] * r[2];  

      // octupole
      o2p[0] = o_v[0] * qc_v[0] * r00 +
               o_v[1] * qc_v[1] * r01 +
               o_v[2] * qc_v[2] * r02 +
               o_v[4] * qc_v[4] * r11 +
               o_v[5] * qc_v[5] * r12 +
               o_v[8] * qc_v[8] * r22; 

      o2p[1] = o_v[1]  * qc_v[0] * r00 +
               o_v[4]  * qc_v[1] * r01 +
               o_v[5]  * qc_v[2] * r02 +
               o_v[13] * qc_v[4] * r11 +
               o_v[14] * qc_v[5] * r12 +
               o_v[17] * qc_v[8] * r22;

      o2p[2] = o_v[2]  * qc_v[0] * r00 +
               o_v[5]  * qc_v[1] * r01 +
               o_v[8]  * qc_v[2] * r02 +
               o_v[14] * qc_v[4] * r11 +
               o_v[17] * qc_v[5] * r12 +
               o_v[26] * qc_v[8] * r22;

      // hexadecapole
      h2p[0] = h_v[0]  * oc_v[0]  *  r[0] * r00 +
               h_v[1]  * oc_v[1]  *  r[0] * r01 +
               h_v[2]  * oc_v[2]  *  r[0] * r02 +
               h_v[4]  * oc_v[4]  *  r[0] * r11 +
               h_v[5]  * oc_v[5]  *  r[0] * r12 +
               h_v[8]  * oc_v[8]  *  r[0] * r22 + 
               h_v[13] * oc_v[13] *  r[1] * r11 +
               h_v[14] * oc_v[14] *  r[1] * r12 +
               h_v[17] * oc_v[17] *  r[1] * r22 +
               h_v[26] * oc_v[26] *  r[2] * r22;
           
      h2p[1] = h_v[1]  * oc_v[0]  * r[0] * r00 +
               h_v[4]  * oc_v[1]  * r[0] * r01 +
               h_v[5]  * oc_v[2]  * r[0] * r02 +
               h_v[13] * oc_v[4]  * r[0] * r11 +
               h_v[14] * oc_v[5]  * r[0] * r12 +
               h_v[17] * oc_v[8]  * r[0] * r22 +
               h_v[40] * oc_v[13] * r[1] * r11 +
               h_v[41] * oc_v[14] * r[1] * r12 +
               h_v[44] * oc_v[17] * r[1] * r22 +
               h_v[53] * oc_v[26] * r[2] * r22;

      h2p[2] = h_v[2]  * oc_v[0]  * r[0] * r00 +
               h_v[5]  * oc_v[1]  * r[0] * r01 +
               h_v[8]  * oc_v[2]  * r[0] * r02 +
               h_v[14] * oc_v[4]  * r[0] * r11 +
               h_v[17] * oc_v[5]  * r[0] * r12 +
               h_v[26] * oc_v[8]  * r[0] * r22 +
               h_v[41] * oc_v[13] * r[1] * r11 +
               h_v[44] * oc_v[14] * r[1] * r12 +
               h_v[53] * oc_v[17] * r[1] * r22 +
               h_v[80] * oc_v[26] * r[2] * r22;

      // rank-2 contraction
      double o3p[9];
      double h3p[9];

      o3p[0] = o_v[0] * r[0] +
               o_v[1] * r[1] +
               o_v[2] * r[2];

      o3p[1] = o_v[1]  * r[0] +
               o_v[4]  * r[1] +
               o_v[5]  * r[2];

      o3p[2] = o_v[2]  * r[0] +
               o_v[5]  * r[1] +
               o_v[8]  * r[2];

      o3p[4] = o_v[4]  * r[0] +
               o_v[13] * r[1] +
               o_v[14] * r[2];

      o3p[5] = o_v[5]  * r[0] +
               o_v[14] * r[1] +
               o_v[17] * r[2];

      o3p[8] = o_v[8]  * r[0] +
               o_v[17] * r[1] +
               o_v[26] * r[2];

      h3p[0] = qc_v[0] * h_v[0] * r00 +
               qc_v[1] * h_v[1] * r01 +
               qc_v[2] * h_v[2] * r02 +
               qc_v[4] * h_v[4] * r11 +
               qc_v[5] * h_v[5] * r12 +
               qc_v[8] * h_v[8] * r22;

      h3p[1] = qc_v[0] * h_v[1]  * r00 +
               qc_v[1] * h_v[4]  * r01 +
               qc_v[2] * h_v[5]  * r02 +
               qc_v[4] * h_v[13] * r11 +
               qc_v[5] * h_v[14] * r12 +
               qc_v[8] * h_v[17] * r22;

      h3p[2] = qc_v[0] * h_v[2]  * r00 +
               qc_v[1] * h_v[5]  * r01 +
               qc_v[2] * h_v[8]  * r02 +
               qc_v[4] * h_v[14] * r11 +
               qc_v[5] * h_v[17] * r12 +
               qc_v[8] * h_v[26] * r22;

      h3p[4] = qc_v[0] * h_v[4]  * r00 +
               qc_v[1] * h_v[13] * r01 +
               qc_v[2] * h_v[14] * r02 +
               qc_v[4] * h_v[40] * r11 +
               qc_v[5] * h_v[41] * r12 +
               qc_v[8] * h_v[44] * r22;

      h3p[5] = qc_v[0] * h_v[5]  * r00 +
               qc_v[1] * h_v[14] * r01 +
               qc_v[2] * h_v[17] * r02 +
               qc_v[4] * h_v[41] * r11 +
               qc_v[5] * h_v[44] * r12 +
               qc_v[8] * h_v[53] * r22;

      h3p[8] = qc_v[0] * h_v[8]  * r00 +
               qc_v[1] * h_v[17] * r01 +
               qc_v[2] * h_v[26] * r02 +
               qc_v[4] * h_v[44] * r11 +
               qc_v[5] * h_v[53] * r12 +
               qc_v[8] * h_v[80] * r22;

      // Loop over ranks
      for (int i = 0; i < 3; i++){
        int ii = i * 4; // Diagonal elements
        e_v[i] -= 3.0 * dp * r[i] / d5  - d_v[i] / d3 +
                  5.0 * qp * r[i] / d7  - 2.0 * q2p[i] / d5 +
                  7.0 * op * r[i] / d9  - 3.0 * o2p[i] / d7 +
                  9.0 * hp * r[i] / d11 - 4.0 * h2p[i] / d9;

        de_v[ii] += -3.0 * dp / d5 - 5.0 * qp / d7 - 7.0 * op / d9 - 9.0 * hp / d11;
        for (int j = i; j < 3; j++){
          // EOJ : Feb/23 for reduced sum set j = i
          int ij = i * 3 + j;
          de_v[ij] += 15.0 * dp * r[i] * r[j] / d7 - 
                      3.0 * (d_v[i] * r[j] + d_v[j] * r[i]) / d5 +
                      35.0 * qp * r[i] * r[j] / d9 -
                      10.0 * (q2p[i] * r[j] + q2p[j] * r[i]) / d7 +
                      2.0 * q_v[ij] / d5 +
                      63.0 * op * r[i] * r[j] / d11 -
                      21.0 * (o2p[i] * r[j] + o2p[j] * r[i]) / d9 +
                      6.0 * o3p[ij] / d7 +
                      99.0 * hp * r[i] * r[j] / d13 -
                      36.0 * (h2p[i] * r[j] + h2p[j] * r[i]) / d11 +
                      12.0 * h3p[ij] / d9;
        }
      }
    }
    int seqq[5];
    int rank;
    for (int a = 0; a < 3; a++){
      for (int b = 0; b < 3; b++){
        seqq[0] = a;
        seqq[1] = b;
        // Arrange for quadrupole
        rank = 2;
        arrange(seqq, rank);
        // quadrupole
        int ab = a * 3 + b;
        int aa = seqq[0] * 3 + seqq[1];
        de_v[ab] = de_v[aa];
      }
    }        
  }
  Py_RETURN_NONE;
}

PyObject *electric_field_com(PyObject *self, PyObject *args)
{
  PyArrayObject* beg_v_obj;
  PyArrayObject* h_v_obj;
  PyArrayObject* e_pv_obj;
  PyArrayObject* de_pv_obj;
  PyArrayObject* R_pv_obj;
  PyArrayObject* dcom_pv_obj;
  PyArrayObject* rhot_G_obj;
  PyArrayObject* Nc_v_obj;
  double lrc;
  double width;
  double damp_f;
  PyArrayObject* pbc_v_obj;
  PyArrayObject* Rc_v_obj;
  PyArrayObject* d2e_pv_obj = 0;
  PyArrayObject* d3e_pv_obj = 0;
  PyArrayObject* d4e_pv_obj = 0;
  if (!PyArg_ParseTuple(args, "OOOOOOOOdddOO|OOO", &beg_v_obj, &h_v_obj, &e_pv_obj,
                        &de_pv_obj, &R_pv_obj, &dcom_pv_obj, &rhot_G_obj, &Nc_v_obj,
                        &lrc, &width, &damp_f, &pbc_v_obj, &Rc_v_obj,
                        &d2e_pv_obj, &d3e_pv_obj, &d4e_pv_obj))
  return NULL;

  const long *beg_v = PyArray_DATA(beg_v_obj);
  const double *h_v = PyArray_DATA(h_v_obj);
  double *e_pv = PyArray_DATA(e_pv_obj);
  double *de_pv = PyArray_DATA(de_pv_obj);
  const double *R_pv = PyArray_DATA(R_pv_obj);
  const double *dcom_pv = PyArray_DATA(dcom_pv_obj);
  const double *rhot_G = PyArray_DATA(rhot_G_obj);
  const long *Nc_v = PyArray_DATA(Nc_v_obj);
  const double *pbc_v = PyArray_DATA(pbc_v_obj);
  const double *Rc_v = PyArray_DATA(Rc_v_obj);

  int np = PyArray_DIM(R_pv_obj, 0);
  npy_intp* n = PyArray_DIMS(rhot_G_obj);
  int noc = PyArray_DIM(Nc_v_obj, 0);

  const double dV = h_v[0] * h_v[1] * h_v[2];
  const double lrc12 = lrc - width;
  double r[3];
  double r_o[3];
  double r_m[noc][3];

  // Handle 3rd-5th derivative of the potential
  double* d2e_pv = 0;
  double* d3e_pv = 0;
  double* d4e_pv = 0;
  if (d2e_pv_obj != 0){
    d2e_pv = PyArray_DATA(d2e_pv_obj);
    d3e_pv = PyArray_DATA(d3e_pv_obj);
    d4e_pv = PyArray_DATA(d4e_pv_obj);
  }

  for (int p = 0; p < np; p++){
    const double* R_v = R_pv + 3 * p;
    double* e_v = e_pv + 3 * p;
    double* de_v = de_pv + 9 * p;
    double* d2e_v = d2e_pv + 27 * p;
    double* d3e_v = d3e_pv + 81 * p;
    double* d4e_v = d4e_pv + 243 * p;

    // We check long range here
    // Long-range cut-off
    const double* dcom_v = dcom_pv + 3 * p;
    double dc, dxc, dyc, dzc;
    // 
    dxc = (1 - pbc_v[0]) * dcom_v[0]; //dcom_v[0] + pbc_v[0] * (r[0] - dcom_v[0]);  
    dyc = (1 - pbc_v[1]) * dcom_v[1];  
    dzc = (1 - pbc_v[2]) * dcom_v[2];
    dc = sqrt(dxc * dxc + dyc * dyc + dzc * dzc);
    // Check long range
    double fsw = 1.0;
    if (dc > lrc) // Outside of lrc; skip k (can't skip p)
      goto cnt_f; // fsw *= 0.0;
    else if (dc > lrc12){
      double xx = (dc - lrc12) / width;
      fsw -= xx * xx * (3.0 - 2.0 * xx);
    }

    for (int i = 0; i < n[0]; i++){
      double x = (beg_v[0] + i) * h_v[0];
      for (int j = 0; j < n[1]; j++){
        double y = (beg_v[1] + j) * h_v[1];
        int ij = (i * n[1] + j) * n[2];
        for (int k = 0; k < n[2]; k++){
          double z = (beg_v[2] + k) * h_v[2];
          int G = ij + k;
          r_o[0] = R_v[0] - x;
          r_o[1] = R_v[1] - y;
          r_o[2] = R_v[2] - z;

          // We don't apply MIC due to cell dipole
          // apply mic
          // mic(r_o, pbc_v, Rc_v);
          // get r matrix
          nn_matrix(Nc_v, r_m, r_o, Rc_v, noc);
          for (int w = 0; w < noc; w++){
            r[0] = r_m[w][0];
            r[1] = r_m[w][1];
            r[2] = r_m[w][2];

            double d = sqrt(r[0] * r[0] + r[1] * r[1] + r[2] * r[2]);
            double d2 = d * d;

            // Calculate electric field and gradient(s)
            double S = d / damp_f;
            double S2 = S * S;
            double S3 = S2 * S;
            double fac;
            double damp_d;
            double damp_q;
            // Damping functions of the Stone type
            fac = 2.0 / sqrt(M_PI) * exp(-S2);
            damp_d = erf(S) - S * fac;
            damp_q = damp_d - 2.0 / 3.0 * S3 * fac;

            // Calculate interaction tensors and apply lrc
            double dot = 1.0 / (d2 * d) * fsw;
            double dof = dot / d2;
            double cd_r = rhot_G[G] * dot * dV * damp_d;
            double cq_r = 3.0 * rhot_G[G] * dof * dV * damp_q;

            // e-field
            e_v[0] -= cd_r * r[0];
            e_v[1] -= cd_r * r[1];
            e_v[2] -= cd_r * r[2];
            // grad e-field
            de_v[0] += cq_r * r[0] * r[0] - cd_r;
            de_v[1] += cq_r * r[0] * r[1];
            de_v[2] += cq_r * r[0] * r[2];
            de_v[3] += cq_r * r[1] * r[0];
            de_v[4] += cq_r * r[1] * r[1] - cd_r;
            de_v[5] += cq_r * r[1] * r[2];
            de_v[6] += cq_r * r[2] * r[0];
            de_v[7] += cq_r * r[2] * r[1];
            de_v[8] += cq_r * r[2] * r[2] - cd_r;

            if (d2e_pv != 0){
              // Doing higher order derivative of field and forces
              double damp_o;
              double damp_h;
              double damp_t;
              double S6 = S3 * S3;

              damp_o = damp_q - 4.0 / 15.0
                       * S3 * S2 * fac;
              damp_h = damp_o - 8.0 / 105.0
                       * S6 * S * fac;
              damp_t = damp_h - 16.0 / 945.0
                       * S6 * S3 * fac;

              double dos = dof / d2;
              double don = dos / d2;
              double dol = don / d2;
              // Contribution from charge density
              double co_r = 15.0 * rhot_G[G] * dos * dV * damp_o;
              double ch_r = 105.0 * rhot_G[G] * don * dV * damp_h;
              double ct_r = 945.0 * rhot_G[G] * dol * dV * damp_t;

              for (int a = 0; a < 3; a++){
                // (aaa..)
                int aaa = a * 13;
                int aaaa = aaa + a * 27;
                int aaaaa = aaaa + a * 81;
                d2e_v[aaa] += cq_r * r[a];
                d3e_v[aaaa] -= co_r * r[a] * r[a] - 2.0 * cq_r;
                d4e_v[aaaaa] += ch_r * r[a] * r[a] * r[a] - 6.0 * co_r * r[a];

                for (int b = a; b < 3; b++){
                  int aab = a * 12 + b;
                  int aaab = aab + a * 27;
                  int aaaab = aaab + a * 81;
                  int abb = a * 9 + b * 4;
                  int abbb = a * 27 + b * 13;
                  int abbbb = a * 81 + b * 40;
                  int aabb = a * 36 + b * 4;
                  int aaabb = aabb + a * 81;
                  int aabbb = abbb + a * 81;
                  d2e_v[aab] += cq_r * r[b];
                  d2e_v[abb] += cq_r * r[a];
                  d3e_v[aaab] -= co_r * r[a] * r[b];
                  d3e_v[abbb] -= co_r * r[a] * r[b];
                  d3e_v[aabb] += cq_r;
                  d4e_v[aaaab] += ch_r * r[a] * r[a] * r[b] - 2.0 * co_r * r[b];
                  d4e_v[abbbb] += ch_r * r[a] * r[b] * r[b] - 2.0 * co_r * r[a];
                  d4e_v[aaabb] -= co_r * r[a];
                  d4e_v[aabbb] -= co_r * r[b];

                  for (int c = b; c < 3; c++){
                    int aabc = a * 36 + b * 3 + c;
                    int aaabc = aabc + a * 81;
                    int abbc = a * 27 + b * 12 + c;
                    int abbbc = a * 81 + b * 39 + c;
                    int abcc = a * 27 + b * 9 + c * 4;
                    int abccc = a * 81 + b * 27 + c * 13;
                    int aabbc = abbc + a * 81;
                    int aabcc = abcc + a * 81;
                    int abbcc = a * 81 + b * 36 + c * 4;
                    d3e_v[aabc] -= co_r * r[b] * r[c];
                    d3e_v[abbc] -= co_r * r[a] * r[c];
                    d3e_v[abcc] -= co_r * r[a] * r[b];
                    d4e_v[aaabc] += ch_r * r[a] * r[b] * r[c];
                    d4e_v[abbbc] += ch_r * r[a] * r[b] * r[c];
                    d4e_v[abccc] += ch_r * r[a] * r[b] * r[c];
                    d4e_v[aabbc] -= co_r * r[c];
                    d4e_v[aabcc] -= co_r * r[b];
                    d4e_v[abbcc] -= co_r * r[a];

                    int abc = a * 9 + b * 3 + c;
                    d2e_v[abc] -= co_r * r[a] * r[b] * r[c];

                    for (int f = c; f < 3; f++){
                      int aabcf = a * 108 + b * 9 + c * 3 + f;
                      int abbcf = a * 81 + b * 36 + c * 3 + f;
                      int abccf = a * 81 + b * 27 + c * 12 + f;
                      int abcff = a * 81 + b * 27 + c * 9 + f * 4;
                      d4e_v[aabcf] += ch_r * r[b] * r[c] * r[f];
                      d4e_v[abbcf] += ch_r * r[a] * r[c] * r[f];
                      d4e_v[abccf] += ch_r * r[a] * r[b] * r[f];
                      d4e_v[abcff] += ch_r * r[a] * r[b] * r[c];

                      int abcf = a * 27 + b * 9 + c * 3 + f;
                      d3e_v[abcf] += ch_r * r[a] * r[b] * r[c] * r[f];

                      for (int h = f; h < 3; h++){
                        int abcfh = a * 81 + b * 27 + c * 9 + f * 3 + h;
                        d4e_v[abcfh] -= ct_r * r[a] * r[b] * r[c] * r[f] * r[h];
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    // Fill in (3^n) matrices here
    if (d2e_pv != 0){
      int seqo[5], seqh[5], seqt[5];
      int rank;
      for (int a = 0; a < 3; a++){
        for (int b = 0; b < 3; b++){
          for (int c = 0; c < 3; c++){
            seqo[2] = c;
            // Arrange for octupole
            seqo[0] = a; seqo[1] = b;
            rank = 3;
            arrange(seqo, rank);
            // Octupole
            int abc = a * 9 + b * 3 + c;
            int aaa = seqo[0] * 9 + seqo[1] * 3 + seqo[2];
            d2e_v[abc] = d2e_v[aaa];
            for (int d = 0; d < 3; d++){
              seqh[3] = d;
              // Arrange for hexadecapole
              seqh[0] = a; seqh[1] = b; seqh[2] = c;
              rank = 4;
              arrange(seqh, rank);
              // Hexadecapole
              int abcd = a * 27 + b * 9 + c * 3 + d;
              int aaaa = seqh[0] * 27 + seqh[1] * 9 + seqh[2] * 3 + seqh[3];
              d3e_v[abcd] = d3e_v[aaaa];
              for (int e = 0; e < 3; e++){
                seqt[4] = e;
                // Arrange for 32-pole
                rank = 5;
                seqt[0] = a; seqt[1] = b;
                seqt[2] = c; seqt[3] = d;
                arrange(seqt, rank);
                // 32-pole
                int abcde = a * 81 + b * 27 + c * 9 + d * 3 + e;
                int aaaaa = seqt[0] * 81 + seqt[1] * 27 + seqt[2] * 9
                            + seqt[3] * 3 + seqt[4];
                d4e_v[abcde] = d4e_v[aaaaa];
              } 
            }
          }
        }
      }
    }
  cnt_f:;  
  }
  Py_RETURN_NONE;
}
