"""This module defines different external potentials."""
import copy
import warnings
from typing import Callable, Dict, Optional

import numpy as np
from ase.units import Bohr, Ha

from gpaw.transformers import Transformer

import _gpaw
from gpaw.typing import Array3D
from gpaw.lfc import LFC

__all__ = ['ConstantPotential', 'ConstantElectricField', 'CDFTPotential',
           'PointChargePotential', 'PolarizableEmbedding', 'StepPotentialz',
           'PotentialCollection']

known_potentials: Dict[str, Callable] = {}


def _register_known_potentials():
    from gpaw.bfield import BField
    known_potentials['CDFTPotential'] = lambda: None  # ???
    known_potentials['BField'] = BField
    for name in __all__:
        known_potentials[name] = globals()[name]


def create_external_potential(name, **kwargs):
    """Construct potential from dict."""
    if not known_potentials:
        _register_known_potentials()
    return known_potentials[name](**kwargs)


class ExternalPotential:
    vext_g: Optional[Array3D] = None
    vext_q: Optional[Array3D] = None

    def get_potential(self, gd):
        """Get the potential on a regular 3-d grid.

        Will only call calculate_potential() the first time."""

        if self.vext_g is None:
            self.calculate_potential(gd)
            self.vext_g.flags.writeable = False
        return self.vext_g

    def get_potentialq(self, gd, pd3):
        """Get the potential on a regular 3-d grid in real space.

        Will only call calculate_potential() the first time."""

        if self.vext_q is None:
            vext_g = self.get_potential(gd)
            self.vext_q = pd3.fft(vext_g)
            self.vext_q.flags.writeable = False

        return self.vext_q

    def calculate_potential(self, gd) -> None:
        raise NotImplementedError

    def get_name(self) -> str:
        return self.__class__.__name__

    def update_potential_pw(self, ham, dens) -> float:
        v_q = self.get_potentialq(ham.finegd, ham.pd3).copy()
        eext = ham.pd3.integrate(v_q, dens.rhot_q, global_integral=False)
        dens.map23.add_to1(ham.vt_Q, v_q)
        ham.vt_sG[:] = ham.pd2.ifft(ham.vt_Q)
        if not ham.collinear:
            ham.vt_xG[1:] = 0.0
        return eext

    def update_atomic_hamiltonians_pw(self, ham, W_aL, dens) -> None:
        vext_q = self.get_potentialq(ham.finegd, ham.pd3)
        dens.ghat.integrate(ham.vHt_q + vext_q, W_aL)

    def paw_correction(self, Delta_p, dH_sp) -> None:
        pass

    def derivative_pw(self, ham, ghat_aLv, dens) -> None:
        vext_q = self.get_potentialq(ham.finegd, ham.pd3)
        dens.ghat.derivative(ham.vHt_q + vext_q, ghat_aLv)


class NoExternalPotential(ExternalPotential):
    vext_g = np.zeros((0, 0, 0))

    def update_potential_pw(self, ham, dens) -> float:
        ham.vt_sG[:] = ham.pd2.ifft(ham.vt_Q)
        if not ham.collinear:
            ham.vt_xG[1:] = 0.0
        return 0.0

    def update_atomic_hamiltonians_pw(self, ham, W_aL, dens):
        dens.ghat.integrate(ham.vHt_q, W_aL)

    def derivative_pw(self, ham, ghat_aLv, dens):
        dens.ghat.derivative(ham.vHt_q, ghat_aLv)


class ConstantPotential(ExternalPotential):
    """Constant potential for tests."""
    def __init__(self, constant=1.0):
        self.constant = constant / Ha
        self.name = 'ConstantPotential'

    def __str__(self):
        return 'Constant potential: {:.3f} V'.format(self.constant * Ha)

    def calculate_potential(self, gd):
        self.vext_g = gd.zeros() + self.constant

    def todict(self):
        return {'name': self.name,
                'constant': self.constant * Ha}


class ConstantElectricField(ExternalPotential):
    def __init__(self, strength, direction=[0, 0, 1], tolerance=1e-7):
        """External constant electric field.

        strength: float
            Field strength in V/Ang.
        direction: vector
            Polarisation direction.
        """
        d_v = np.asarray(direction)
        self.field_v = strength * d_v / (d_v**2).sum()**0.5 * Bohr / Ha
        self.tolerance = tolerance
        self.name = 'ConstantElectricField'

    def __str__(self):
        return ('Constant electric field: '
                '({:.3f}, {:.3f}, {:.3f}) V/Ang'
                .format(*(self.field_v * Ha / Bohr)))

    def calculate_potential(self, gd):
        # Currently skipped, PW mode is periodic in all directions
        # d_v = self.field_v / (self.field_v**2).sum()**0.5
        # for axis_v in gd.cell_cv[gd.pbc_c]:
        #     if abs(np.dot(d_v, axis_v)) > self.tolerance:
        #         raise ValueError(
        #             'Field not perpendicular to periodic axis: {}'
        #             .format(axis_v))

        center_v = 0.5 * gd.cell_cv.sum(0)
        r_gv = gd.get_grid_point_coordinates().transpose((1, 2, 3, 0))
        self.vext_g = np.dot(r_gv - center_v, self.field_v)

    def todict(self):
        strength = (self.field_v**2).sum()**0.5
        return {'name': self.name,
                'strength': strength * Ha / Bohr,
                'direction': self.field_v / strength}


class ProductPotential(ExternalPotential):
    def __init__(self, ext_i):
        self.ext_i = ext_i

    def calculate_potential(self, gd):
        self.vext_g = self.ext_i[0].get_potential(gd).copy()
        for ext in self.ext_i[1:]:
            self.vext_g *= ext.get_potential(gd)

    def __str__(self):
        return '\n'.join(['Product of potentials:'] +
                         [ext.__str__() for ext in self.ext_i])

    def todict(self):
        return {'name': self.__class__.__name__,
                'ext_i': [ext.todict() for ext in self.ext_i]}

# Loading in specifically for PE
from gpaw.utilities.timing import Timer
from math import sqrt, pi
         
import _gpaw
from gpaw import debug

import gpaw.mpi as mpi
         
# multipole moments - reduced arrays unique elements
MF = {'d':np.array([0,1,2]),
      'q':np.array([0,1,2,4,5,8]),
      'o':np.array([0,1,2,4,5,8,13,14,17,26]),
      'h':np.array([0,1,2,4,5,8,13,14,17,26,
                    40,41,44,53,80]),
      't':np.array([0,1,2,4,5,8,13,14,17,26,
                    40,41,44,53,80,121,122,
                    125,134,161,243])}
         
# multipole moments - multipliers for unique elements
MM = {'d':np.array([1,1,1], dtype=float),
      'q':np.array([1,2,2,1,2,1], dtype=float),
      'o':np.array([1,3,3,3,6,3,1,3,3,1], dtype=float),
      'h':np.array([1,4,4,6,12,6,4,12,12,4,1,4,6,4,1], dtype=float),
      't':np.array([1,5,5,10,20,10,10,30,30,10,5,20,
                    30,20,5,1,5,10,10,5,1], dtype=float)}

tc_M = np.zeros(81)
tc_M[MF['h']] = MM['h']

oc_M = np.zeros(27)
oc_M[MF['o']] = MM['o']
hc_M = np.zeros(81)
hc_M[MF['h']] = MM['h']


class PolarizableEmbedding(ExternalPotential):
    # EOJ : TODO; Make sure units passed into this class are in Bohr.
    def __init__(self, mm, qm, mp, calcmm, lrc=11.0, width=2.0, 
                 damping_value=0.75,
                 grid_mask=None, Nc=np.array([0,0,0]), 
                 Nc_cg=np.array([0,0,0]), Rc=None,
                 grid_buffer=1.0, **parameters):
        """ Class to handle external potential due to polarizable
            multipoles from SCME type calculator object.

            mm : Atoms object
               for MM subsystem.

            qm : Atoms object
               for QM subsystem.

            mp : integer
               no. atoms per molecule in MM subsystem.

            calcmm : Calculator object of type SCME
               for MM subsystem.

            lrc : float [Bohr]
               long range cut-off for QM/MM electrostatic int.

            width : float [Bohr]
               width of the smooth long range cut-off.

            damping_value : float [Bohr**-1]

            Nc : (1, 3)-shape array of integers
               number of near neighbor images along periodic dims.

            EOJ : perhaps have Nc : (#, 3) where # is 1-3 and represent
               levels of coarse-ness...

            Nc_cg : (1, 3)-shape array of integers
               number of near neighbor images along periodic dims
               which are represented with a coarse grid.

            Rc : (1, 3)-shape array of floats [Bohr]
               Origin in QM region for the long-range cut-off measure.

            grid_buffer : float [Bohr]
               Buffer region to determine whether potential due to MM
               site i is evaluated on the coarse grid.

        """
        self._dict = dict(name=self.__class__.__name__,
                          mm=mm, qm=qm, mp=mp, calcmm=calcmm,
                          damping_value=damping_value,
                          grid_mask=grid_mask, Nc=Nc)

        # MM subsystem
        self.mm     = mm
        self.mp     = mp
        self.calcmm = calcmm
        assert calcmm.get_name() == 'SCME', \
        'MM calculator is not of the SCME type'

        self.nm     = int(len(self.mm) // self.mp) # no. MM mols
        self.cm     = self.get_mm_coms(self.mm)
        
        # QM subsystem
        self.qm     = qm
        self.qmidx  = int(len(qm)) # no. QM atoms
        
        self.timer  = Timer()

        self.damping_value = damping_value
        self.grid_buffer   = grid_buffer / Bohr

        # Long range cut-off and width in Bohr
        self.lrc   = lrc / Bohr
        self.width = width / Bohr

        # PBC logics
        assert (self.qm.pbc == [Nc != 0]).all(), \
        'PBC of QM system does not match the near-neighbor grid!'
        self.pbc = np.ones(3) * self.qm.pbc # Have to be 1/0

        # Near-neighbor images
        self.Nc = Nc
        self.Nc_cg = Nc_cg

        # Make grid(s)
        self.NNc = self.make_nn_grid(self.Nc)
        self.NNc_cg = self.make_nn_grid(self.Nc_cg, self.NNc)

        # Make total grid
        if (self.Nc_cg > 0).any():
            self.NNt = self.make_nn_grid(self.Nc_cg)
        else:
            self.NNt = self.make_nn_grid(self.Nc)

        # Long-range cut-off measure
        if Rc:
            self.Rc = Rc / Bohr
        else:
            # Place origin at qm center
            self.Rc = self.qm.cell.diagonal() / 2.0 / Bohr

        # Side lengths for MIC
        self.L  = self.qm.cell.diagonal() / Bohr

        # Hold on to new and old dipole/qpole arrays
        self.dpoles = None
        self.qpoles = None
        self.dpoles_1 = None
        self.qpoles_1 = None

        # Hold on to higher order potential (octu- and hexadecapole)
        self.potential_oh = None

        #
        self.vext_g = None
        self.dummy  = False

        # Pseudo core charge on coarse grid (possibly obsolete)
        self.ghat_cg = None

        self.initial = True
    
    def initialize_arrays(self, dens):
        # Gradients at each MM site
        self.d1v = np.ascontiguous(np.zeros((self.nm, 3)))
        self.d2v = np.ascontiguous(np.zeros((self.nm, 3, 3)))
        self.d3v = np.ascontiguous(np.zeros((self.nm, 3, 3, 3)))
        self.d4v = np.ascontiguous(np.zeros((self.nm, 3, 3, 3, 3)))
        self.d5v = np.ascontiguous(np.zeros((self.nm, 3, 3, 3, 3, 3)))

        # External potential due to MM sites
        self.potential = dens.finegd.zeros()
        self.potential_oh = dens.finegd.zeros()

        # List together grid descriptors, masks, potential etc.
        self.gd = [dens.finegd]
        self.gm = [self.grid_mask]
        self.nn = [self.NNc]

        if (~self.grid_mask).any():
            self.potential_cg = dens.gd.zeros()
            self.potential_oh_cg = dens.gd.zeros()
            self.gd.append(dens.gd)
            self.gm.append(~self.grid_mask)
            self.nn.append(self.NNt)

        if (self.Nc_cg > 0).any():
            self.potential_nn = dens.gd.zeros()
            self.potential_oh_cg = dens.gd.zeros()
            self.gd.append(dens.gd)
            self.gm.append(self.grid_mask)
            self.nn.append(self.NNc_cg)

        pass

    def make_nn_grid(self, Nc, cmpr=None):
        """ Make a grid in integers corresponding
            to near-neighbor images.
        """
        NN = []
        g = np.zeros(3)

        for i in range(-Nc[0],Nc[0]+1,1):
            for j in range(-Nc[1],Nc[1]+1,1):
                for k in range(-Nc[2],Nc[2]+1,1):
                    g[0] = i
                    g[1] = j
                    g[2] = k

                    if cmpr is not None:
                       if ~np.array([(g == nn).all() for i, nn in enumerate(cmpr)]).any():
                           NN.append(g)
                    else:
                       NN.append(g)
                    g = np.zeros(3)

        return np.array(NN, dtype=int)

    # get_potential supersedes ExternalPotential / need PW equivalent...
    def get_potential(self, gd=None, density=None,
                      setups=None, nspins=None, forces=False):
        """ Create external potential from dipoles
            up to and including hexadecapole with 
            origin at the center of mass of each 
            SCME water molecule in the MM atoms object
        """
        if False in self.grid_mask:
           # Need control function for gm
           if (self.Nc_cg > 0).any():
               self.NNc_gm = self.make_nn_grid(self.Nc_cg)
           else:
               self.NNc_gm = self.make_nn_grid(self.Nc)


        if self.initial:
            if (False in self.grid_mask) or (self.Nc_cg > 0).any():
                #print('here important')

                self.ghat_cg = LFC(density.gd, 
                                   [setup.ghat_l for setup in setups],
                                   integral=sqrt(4 * pi), 
                                   forces=True)

                spos_ac = self.qm.get_scaled_positions() % 1.0
                #self.ghat_cg.set_positions(setups.spos_ac)
                self.ghat_cg.set_positions(spos_ac, density.atom_partition)
                # Need to apply restrictor; Above is not working correctly.
                self.anti_interpolator = Transformer(density.finegd, density.gd)
                self.interpolator = Transformer(density.gd, density.finegd)

            # Update dynamic part of potential
            self.update_potential(gd=gd, density=density,
                                  setups=setups, nspins=nspins)
            self.vext_g = self.potential.copy()
            # Add static part of potential
            self.vext_g += self.potential_oh
            return self.vext_g

        elif not self.dummy:
            #
            if not self.check_convergence(density=density):
                self.update_potential(gd=gd, 
                                      density=density,
                                      setups=setups, 
                                      nspins=nspins)
            else:  # when converged update the non-induced Eoh
                # one more scme calc with inscf=False -- WHY???
                # Bit hacky, but allowing for DFT-D3 
                if 'convergence' in self.qm.calc.parameters:
                    cc = self.qm.calc.parameters['convergence']['dpoles']
                else:
                    cc = self.qm.calc.dft.parameters['convergence']['dpoles']
         
                self.calcmm.calculate(self.mm, 
                                      denserror=cc, 
                                      inscf=False)
         
            self.vext_g = self.potential.copy()
            # Add static part of potential
            self.vext_g += self.potential_oh

            if forces:
                # Eval all electric field terms with conv. density
                d1v, d2v, d3v, d4v, d5v = self.get_efield(density, 
                                                          setups, 
                                                          nspins, 
                                                          forces=forces)
                #####################################
                # Pass values to SCME
                " --> Needs send-receive "
                mm = self.mm
                calcmm = self.calcmm
                calcmm.d1v = d1v
                calcmm.d2v = d2v
                calcmm.d3v = d3v
                calcmm.d4v = d4v
                calcmm.d5v = d5v
                # Make sure SCME uses the fields and derivatives
                # Bit hacky, but allowing for DFT-D3 
                if 'convergence' in self.qm.calc.parameters:
                    cc = self.qm.calc.parameters['convergence']['dpoles']
                else:
                    cc = self.qm.calc.dft.parameters['convergence']['dpoles']
                calcmm.calculate(mm, 
                                 denserror=cc, 
                                 inscf=False)
        
            return self.vext_g
        
        else:
            if hasattr(self, 'potential'):
                if gd == self.gd or gd is None:
                # Nothing changed
                    return self.vext_g

    def update_potential(self, gd=None, density=None,
                         setups=None, nspins=None):
 
        # Make sure old arrays are used
        if gd is not None:
            self.gd = gd 
                         
        if density is not None:
            self.density = density

        if self.cm_gc is None:
            self.cm_gc = self._molecule_distances(self.cm, self.gd)

        # Save old dipoles (for induction convergence check)
        if self.dpoles is not None:
            self.dpoles_1 = self.dpoles.copy()
            self.qpoles_1  = self.qpoles.copy()
                         
        # Grab electric field and derivative values
        self.timer.start('Electric Field and Derivative')
        d1v, d2v, d3v, d4v, d5v = self.get_efield(self.density, 
                                                  setups, 
                                                  nspins)
        self.timer.stop('Electric Field and Derivative')
                         
        calcmm = self.calcmm
        mm = self.mm     
                         
        #####################################
        # Pass values to SCME
        calcmm.d1v = d1v   
        calcmm.d2v = d2v
        calcmm.d3v = d3v
        calcmm.d4v = d4v
        calcmm.d5v = d5v
        self.timer.start('SCME Calculation')
        calcmm.calculate(mm, 
                         denserror=self.density.error, 
                         inscf=True)
        self.timer.stop('SCME Calculation')
        #####################################
                         
        # Values are in atomic-units
        dpoles = calcmm.dpoles
        qpoles = calcmm.qpoles
        opoles = calcmm.opoles
        hpoles = calcmm.hpoles
                         
        potential = np.ascontiguousarray(np.zeros(gd.end_c-gd.beg_c))
        cgd = self.density.gd
        self.timer.start('Evaluate Potential')
        # Check octo / or make empty potential -- potential by default made on
        # finegd - only make it with the finegd for masked cm's
        mask = self.grid_mask
        if self.potential_oh is None:
            self.potential_oh = np.ascontiguousarray(np.zeros(gd.end_c-gd.beg_c))
            _gpaw.multipole_potential(gd.beg_c, 
                                      np.ascontiguousarray(gd.h_cv.diagonal().copy()),
                                      dpoles[mask], qpoles[mask], self.cm[mask],
                                      self.cm_gc[mask],
                                      potential, self.NNc, self.lrc, self.width, 
                                      self.damping_value, self.pbc, self.L,
                                      self.potential_oh, opoles[mask], hpoles[mask],
                                      MM['o'], tc_M)
        else:            
            _gpaw.multipole_potential(gd.beg_c, 
                                      np.ascontiguousarray(gd.h_cv.diagonal().copy()),
                                      dpoles[mask], qpoles[mask], self.cm[mask],
                                      self.cm_gc[mask],
                                      potential, self.NNc,
                                      self.lrc, self.width,
                                      self.damping_value, self.pbc, self.L)
                         
        # Some MMs are outside of buffer zone
        if False in mask:
            #print('here')
            potential_cg = np.ascontiguousarray(np.zeros(cgd.end_c-cgd.beg_c))
            if self.initial:
                potential_coh = np.ascontiguousarray(np.zeros(cgd.end_c-cgd.beg_c))
                _gpaw.multipole_potential(cgd.beg_c, 
                                      np.ascontiguousarray(cgd.h_cv.diagonal().copy()),
                                      dpoles[~mask], qpoles[~mask], self.cm[~mask],
                                      self.cm_gc[~mask],
                                      potential_cg, self.NNc_gm,
                                      self.lrc, self.width, 
                                      self.damping_value, self.pbc, self.L,
                                      potential_coh, opoles[~mask], hpoles[~mask],
                                      MM['o'], tc_M)
            else:        
                _gpaw.multipole_potential(cgd.beg_c, 
                                      np.ascontiguousarray(cgd.h_cv.diagonal().copy()),
                                      dpoles[~mask], qpoles[~mask], self.cm[~mask],
                                      self.cm_gc[~mask],
                                      potential_cg, self.NNc_gm,
                                      self.lrc, self.width,
                                      self.damping_value, self.pbc, self.L)
        
        # Most NNs are treated on a coarse grid
        if (self.Nc_cg > 0).any():
            potential_nn_cg = np.ascontiguousarray(np.zeros(cgd.end_c-cgd.beg_c))
            if self.initial:   
                potential_nn_coh = np.ascontiguousarray(np.zeros(cgd.end_c-cgd.beg_c))
                _gpaw.multipole_potential(cgd.beg_c, 
                                      np.ascontiguousarray(cgd.h_cv.diagonal().copy()),
                                      dpoles[mask], qpoles[mask], self.cm[mask],
                                      self.cm_gc[mask],
                                      potential_nn_cg, self.NNc_cg,
                                      self.lrc, self.width, 
                                      self.damping_value, self.pbc, self.L,
                                      potential_nn_coh, opoles[mask], hpoles[mask],
                                      MM['o'], tc_M)
            else:                                   
                _gpaw.multipole_potential(cgd.beg_c,          
                                      np.ascontiguousarray(cgd.h_cv.diagonal().copy()),
                                      dpoles[mask], qpoles[mask], self.cm[mask],
                                      self.cm_gc[mask],      
                                      potential_nn_cg, self.NNc_cg,  
                                      self.lrc, self.width,   
                                      self.damping_value, self.pbc, self.L)

        self.timer.stop('Evaluate Potential')
                         
        # Interpolate if False in mask
        if False in mask:
            #print('here')
            if self.initial:
                f_potential_oh = density.finegd.empty()
                f_potential = density.finegd.empty()
                density.interpolator.apply(potential_coh, f_potential_oh)
                density.interpolator.apply(potential_cg, f_potential)
                potential += f_potential
                self.potential_oh += f_potential_oh
            else:        
                f_potential = density.finegd.empty()
                density.interpolator.apply(potential_cg, f_potential)
                potential += f_potential

        if (self.Nc_cg > 0).any():
             #print('here 1')
             if self.initial:           
                 f_potential_nn_oh = density.finegd.empty()
                 f_potential_nn = density.finegd.empty()         
                 density.interpolator.apply(potential_nn_coh, f_potential_nn_oh)
                 density.interpolator.apply(potential_nn_cg, f_potential_nn)
                 potential += f_potential_nn          
                 self.potential_oh += f_potential_nn_oh 
             else:                      
                 f_potential_nn = density.finegd.empty()         
                 density.interpolator.apply(potential_nn_cg, f_potential_nn)
                 potential += f_potential_nn

        # Potential updated
        self.initial = False
                         
        self.potential = potential.copy()

        # Hold on to
        self.gd = gd
        self.d1v = d1v
        self.d2v = d2v
        self.qpoles = qpoles.copy()
        self.dpoles = dpoles.copy()

    def check_convergence(self, density=None): 
        if density != None:                  
            self.density = density           
                                             
        # Bit hacky, but allowing for DFT-D3 
        if 'convergence' in self.qm.calc.parameters:                     
            convcrit = self.qm.calc.parameters['convergence']['dpoles']  
            convcritq = self.qm.calc.parameters['convergence']['qpoles'] 
        else:  # then its probably DFTD3 and if not it should break anyways 
            convcrit = self.qm.calc.dft.parameters['convergence']['dpoles']
            convcritq = self.qm.calc.dft.parameters['convergence']['qpoles']
                                             
        if self.dpoles_1 is not None:        
            maxdip, maxqua = self.check_difference()                     
            converged = (maxdip < convcrit) and (maxqua < convcritq)     
                                             
            return converged                 
        else:                                
            return False

    def coarse_charge_density(self, density): 
        rhot_g = density.rhot_g

        gd = density.finegd.coarsen()
        restrict = Transformer(density.finegd, gd, 3)

        rhot_G = gd.empty()
        restrict.apply(rhot_g, rhot_G)

        # Remove residual charge
        actual_charge = gd.integrate(rhot_G)
        background = (actual_charge / gd.dv /
                      gd.get_size_of_global_array().prod())
        rhot_G -= background

        # Routines for coarse^2
        # gd2 = gd.coarsen()                                    
        # restrict2 = Transformer(gd, gd2)
                      
        # rhot_G2 = gd2.empty()
                      
        #restrict2.apply(rhot_G, rhot_GG)
        _Q, Q_aL = density.calculate_multipole_moments()

        for a, Q_L in Q_aL.items():
            Q_L[:] *= 0.0

        Q_v_cg = np.zeros((3,3))
        R_c = self.qm.get_positions()[12] / Bohr

        _gpaw.calculate_quadrupole(density.gd.beg_c, density.gd.h_cv.diagonal().copy(),
                                   R_c, rhot_G, Q_v_cg)

        density.gd.comm.sum(Q_v_cg)
        if mpi.world.rank == 0:
            print("Q cg", Q_v_cg)

        Q_v = np.zeros((3,3))
        _gpaw.calculate_quadrupole(density.finegd.beg_c, density.finegd.h_cv.diagonal().copy(),
                                   R_c, density.rhot_g, Q_v)

        density.finegd.comm.sum(Q_v)
        if mpi.world.rank == 0:
            print("Q", Q_v)
            print("dQ", Q_v_cg - Q_v)

        # Need dQ_a at reference atom
        dQ_a = np.zeros((len(self.qm),3,3))
        pos = self.qm.get_positions() / Bohr

        for a, val in enumerate(pos):
            Q_v_cg = np.zeros((3,3))
            _gpaw.calculate_quadrupole(density.gd.beg_c, density.gd.h_cv.diagonal().copy(),
                                     val, rhot_G, Q_v_cg)
            #
            Q_v = np.zeros((3,3))
            _gpaw.calculate_quadrupole(density.finegd.beg_c, density.finegd.h_cv.diagonal().copy(),
                                     val, density.rhot_g, Q_v)
            #
            dQ = Q_v_cg - Q_v
            density.gd.comm.sum(dQ)
            #
            dQ_a[a] = dQ

        # Coefficients according to Rostgaard
        f1 = sqrt(15.0 / (4.0 * pi))   * 2.0 /  3.0
        f2 = sqrt(5.0 /  (16.0 * pi))  * 2.0 / 1.0
        f3 = sqrt(15.0 / (16.0 * pi))  * 2.0 / 3.0

        for a, Q_L in Q_aL.items(): # sqrt(3/4pi) (y,z,x)
             if a == 12:
                 # Q - poles
                 Q_L[4] -= dQ_a[a][0][1] * f1
                 Q_L[5] -= dQ_a[a][1][2] * f1
                 Q_L[6] -= dQ_a[a][2][2] * f2
                 Q_L[7] -= dQ_a[a][0][2] * f1
                 Q_L[8] -= (dQ_a[a][0][0] - dQ_a[a][1][1]) * f3

        # We add quadrupole correction
        self.ghat_cg.add(rhot_G, Q_aL)
        
        d_v_cg = density.gd.calculate_dipole_moment(rhot_G)
        d_v = density.finegd.calculate_dipole_moment(density.rhot_g)
        delta = d_v_cg - d_v

        for a, Q_L in Q_aL.items(): # sqrt(3/4pi) (y,z,x)
              if a == 12:                         
                 Q_L[1] += delta[1] * 0.4886025119029199
                 Q_L[2] += delta[2] * 0.4886025119029199
                 Q_L[3] += delta[0] * 0.4886025119029199
                 # Q - poles                     
                 Q_L[4] *= 0.0 #dQ_a[a][0][1] * f1#* f * f1
                 Q_L[5] *= 0.0 #dQ_a[a][1][2] * f1 #* f * f1
                 Q_L[6] *= 0.0 #dQ_a[a][2][2] * f2#* f
                 Q_L[7] *= 0.0 #dQ_a[a][0][2] * f1#* f * f1
                 Q_L[8] *= 0.0 #(dQ_a[a][0][0] - dQ_a[a][1][1]) * f3#* f * f2

        self.ghat_cg.add(rhot_G, Q_aL)
        #
        d_v_cg = density.gd.calculate_dipole_moment(rhot_G)
        chg = density.gd.comm.sum(rhot_G.sum())

        if mpi.world.rank == 0:
            print('d cg', d_v_cg)
            print('d', d_v)
            print('res', chg)

        Q_v_cg = np.zeros((3,3))         
        R_c = np.array([0.0,0.0,0.0])    
                                         
        _gpaw.calculate_quadrupole(density.gd.beg_c, density.gd.h_cv.diagonal().copy(),
                                   R_c, rhot_G, Q_v_cg) 
                                         
        density.gd.comm.sum(Q_v_cg)      
        if mpi.world.rank == 0:          
            print("Q cg", Q_v_cg)        
                                                        
        Q_v = np.zeros((3,3))            
        # TEST _gpaw.calculate_quadrupole
        _gpaw.calculate_quadrupole(density.finegd.beg_c, density.finegd.h_cv.diagonal().copy(),
                                   R_c, density.rhot_g, Q_v)
                                         
        density.finegd.comm.sum(Q_v)     
        if mpi.world.rank == 0:          
            print("Q", Q_v)              
            print("dQ", Q_v_cg - Q_v)

        return rhot_G

    def check_difference(self):              
        if self.dpoles_1 is not None:        
            dip = abs(self.dpoles - self.dpoles_1 + 1e-12).sum(axis=1)   
                                             
            normq = np.array([np.linalg.norm(q) for q in self.qpoles])   
            normq_1 = np.array([np.linalg.norm(q) for q in self.qpoles_1])
            qua = abs(normq - normq_1 + 1e-12)
                                               
            return np.max([dip]), np.max([qua]) 
        else:                                  
            return np.nan, np.nan  # first round

    def get_efield(self, density, setups, nspins, forces=False):
        """ Evaluate electric field at each cm
            from total psuedo charge density - on fine or coarse grid
        """ 
        gd = self.gd
        #        
        d1v = np.ascontiguousarray(np.zeros((self.nm,3)))
        d2v = np.ascontiguousarray(np.zeros((self.nm,3,3)))
        d3v = np.ascontiguousarray(np.zeros((self.nm,3,3,3)))
        d4v = np.ascontiguousarray(np.zeros((self.nm,3,3,3,3)))
        d5v = np.ascontiguousarray(np.zeros((self.nm,3,3,3,3,3)))
                 
        mask = self.grid_mask
                 
        d1v_fg = d1v[mask].copy()
        d2v_fg = d2v[mask].copy()
        d3v_fg = d3v[mask].copy()
        d4v_fg = d4v[mask].copy()
        d5v_fg = d5v[mask].copy()       
                 
        # Outside of lrc       
        if (False in mask) or (self.Nc_cg > 0).any():
            cgd = density.gd
            rhot_G = self.coarse_charge_density(density)
            #

        if False in mask:
            d1v_cg = d1v[~mask].copy()
            d2v_cg = d2v[~mask].copy()
            d3v_cg = d3v[~mask].copy()
            d4v_cg = d4v[~mask].copy()
            d5v_cg = d5v[~mask].copy()
        # NN        
        if (self.Nc_cg > 0).any():
            #print('here 2')
            #                      
            d1v_nn_cg = d1v[mask].copy()
            d2v_nn_cg = d2v[mask].copy()
            d3v_nn_cg = d3v[mask].copy()
            d4v_nn_cg = d4v[mask].copy()
            d5v_nn_cg = d5v[mask].copy()


        if not forces:
            _gpaw.electric_field_com(gd.beg_c, 
                                     np.ascontiguousarray(gd.h_cv.diagonal().copy()),
                                     d1v_fg, d2v_fg, self.cm[mask],
                                     self.cm_gc[mask],
                                     density.rhot_g,
                                     self.NNc,
                                     self.lrc, self.width,
                                     self.damping_value, self.pbc, self.L)
        else:    
            _gpaw.electric_field_com(gd.beg_c, 
                                     np.ascontiguousarray(gd.h_cv.diagonal().copy()),
                                     d1v_fg, d2v_fg, self.cm[mask],
                                     self.cm_gc[mask],
                                     density.rhot_g,
                                     self.NNc,
                                     self.lrc, self.width,
                                     self.damping_value, self.pbc, self.L,
                                     d3v_fg, d4v_fg, d5v_fg)

        # Collect over domains
        gd.comm.sum(d1v_fg)
        gd.comm.sum(d2v_fg)
        gd.comm.sum(d3v_fg)
        gd.comm.sum(d4v_fg)
        gd.comm.sum(d5v_fg)
                 
        d1v[mask] += d1v_fg
        d2v[mask] += d2v_fg
        d3v[mask] += d3v_fg
        d4v[mask] += d4v_fg
        d5v[mask] += d5v_fg
                 
                 
        if False in mask:
            if not forces:
                _gpaw.electric_field_com(cgd.beg_c, 
                                         np.ascontiguousarray(cgd.h_cv.diagonal().copy()),
                                         d1v_cg, d2v_cg, self.cm[~mask],
                                         self.cm_gc[~mask],
                                         rhot_G,
                                         self.NNc_gm,
                                         self.lrc, self.width,
                                         self.damping_value, self.pbc, self.L)
            else:
                _gpaw.electric_field_com(cgd.beg_c, 
                                         np.ascontiguousarray(cgd.h_cv.diagonal().copy()),
                                         d1v_cg, d2v_cg, self.cm[~mask],
                                         self.cm_gc[~mask],
                                         rhot_G,
                                         self.NNc_gm,
                                         self.lrc, self.width,
                                         self.damping_value, self.pbc, self.L,
                                         d3v_cg, d4v_cg, d5v_cg)
                 
            cgd.comm.sum(d1v_cg)
            cgd.comm.sum(d2v_cg)
            cgd.comm.sum(d3v_cg)
            cgd.comm.sum(d4v_cg)
            cgd.comm.sum(d5v_cg)
                 
            d1v[~mask] += d1v_cg
            d2v[~mask] += d2v_cg
            d3v[~mask] += d3v_cg
            d4v[~mask] += d4v_cg
            d5v[~mask] += d5v_cg

        if (self.Nc_cg > 0).any():
            if not forces:
                #print('here 3')
                _gpaw.electric_field_com(cgd.beg_c, 
                                         np.ascontiguousarray(cgd.h_cv.diagonal().copy()),
                                         d1v_nn_cg, d2v_nn_cg, self.cm[mask],
                                         self.cm_gc[mask],
                                         rhot_G,
                                         self.NNc_cg,
                                         self.lrc, self.width,
                                         self.damping_value, self.pbc, self.L)
            else:
                _gpaw.electric_field_com(cgd.beg_c, 
                                         np.ascontiguousarray(cgd.h_cv.diagonal().copy()),
                                         d1v_nn_cg, d2v_nn_cg, self.cm[mask],
                                         self.cm_gc[mask],
                                         rhot_G,
                                         self.NNc_cg,
                                         self.lrc, self.width,
                                         self.damping_value, self.pbc, self.L,
                                         d3v_nn_cg, d4v_nn_cg, d5v_nn_cg)
                 
            cgd.comm.sum(d1v_nn_cg)
            cgd.comm.sum(d2v_nn_cg)
            cgd.comm.sum(d3v_nn_cg)
            cgd.comm.sum(d4v_nn_cg)
            cgd.comm.sum(d5v_nn_cg)
                 
            d1v[mask] += d1v_nn_cg
            d2v[mask] += d2v_nn_cg
            d3v[mask] += d3v_nn_cg
            d4v[mask] += d4v_nn_cg
            d5v[mask] += d5v_nn_cg

        self.d1v = d1v 
        self.d2v = d2v
        self.d3v = d3v
        self.d4v = d4v
        self.d5v = d5v
                 
        # Convert to SCME-fortran arrays;
        return d1v.transpose(1,0), d2v.transpose(1,2,0), d3v.transpose(1,2,3,0),\
               d4v.transpose(1,2,3,4,0), d5v.transpose(1,2,3,4,5,0)

    def update_atoms(self, mmatoms=None, qmatoms=None, mmcalc=None, grid_mask=None):
        """ Called from ase.calculator.qmmm - updates to the total system
            requires reset of everything here ... Grid mask functionality
            is in ase.calculator.qmmm...
        """
        self.mm = None
        self.qm = None
        self.calcmm = None
        self.dpoles_1 = None
        self.grid_mask = None
        #        
        self.mm = mmatoms
        self.qm = qmatoms
        self.calcmm = mmcalc
        self.cm = self.get_cm(self.nm)

        self.grid_mask = grid_mask
                 
        self.initial = True
        self.vext_g  = None
        self.potential_oh = None
        self.cm_gc = None

    def todict(self):             
        return self._dict
                                  
    def __str__(self):            
        return ('Polarizable embedding potential '
                '(sites: {}, damping: {:.3f} Ang**-1)'
                .format(self.nm,
                        self.damping_value))

    def get_cm(self, n):
        """ Return center of mass in units Bohr
        """
        cm   = np.zeros((int(n), 3))
        atoms = self.mm
        mp = self.mp
     
        for i in range(int(n)):
            cm[i,:] += atoms[i*mp:(i+1)*mp].get_center_of_mass() / Bohr
        return cm

    def _molecule_distances(self, cm, gd):
        return cm - gd.cell_cv.sum(0) / 2

    def get_mm_coms(self, mm):
        cm_MM = []
        mp = self.mp
        for i in range(self.nm):
            cm_MM.append(mm[i*mp:(i+1)*mp].get_center_of_mass() / Bohr)

        return np.array(cm_MM)

    def set_grid_mask(self):
        """ Determine if MM molecules are far enough
            away from the Rc[~pbc] such that the vext and 
            fields at each MM site can be evaluate 
            with the coarse grid.
        """
        # EOJ : check the logics of this for non-PBC vs 2D-PBC
        # EOJ : IF dist is greater than the length of the vector from (0,0,0)
        #     : to the (1/2x,1/2y,1/2z) - but only if non-pbc
        r  = np.sqrt((self.cm[~self.pbc] - 
                      self.Rc[~self.pbc])**2).sum(axis=1)
        r_cut = (self.Rc[~self.pbc].sum() / len(self.Rc[~self.pbc]) + 
                 self.grid_buffer)
        
        return r <= r_cut
        
    def get_molecule_distance(self):          
        """ Array holding position of MM sites
            relative to the long-range cut off
            origin.      
        """              
        return self.cm - self.Rc              
                         
    def get_layers(self):
        """ Determine grid space layers       
            corresponding grid descriptors    
            and corresponding masks           
        """              
        pass             



class PointChargePotential(ExternalPotential):
    def __init__(self, charges, positions=None,
                 rc=0.2, rc2=np.inf, width=1.0):
        """Point-charge potential.

        charges: list of float
            Charges.
        positions: (N, 3)-shaped array-like of float
            Positions of charges in Angstrom.  Can be set later.
        rc: float
            Inner cutoff for Coulomb potential in Angstrom.
        rc2: float
            Outer cutoff for Coulomb potential in Angstrom.
        width: float
            Width for cutoff function for Coulomb part.

        For r < rc, 1 / r is replaced by a third order polynomial in r^2 that
        has matching value, first derivative, second derivative and integral.

        For rc2 - width < r < rc2, 1 / r is multiplied by a smooth cutoff
        function (a third order polynomium in r).

        You can also give rc a negative value.  In that case, this formula
        is used::

            (r^4 - rc^4) / (r^5 - |rc|^5)

        for all values of r - no cutoff at rc2!
        """
        self._dict = dict(name=self.__class__.__name__,
                          charges=charges, positions=positions,
                          rc=rc, rc2=rc2, width=width)
        self.q_p = np.ascontiguousarray(charges, float)
        self.rc = rc / Bohr
        self.rc2 = rc2 / Bohr
        self.width = width / Bohr
        if positions is not None:
            self.set_positions(positions)
        else:
            self.R_pv = None

        if abs(self.q_p).max() < 1e-14:
            warnings.warn('No charges!')
        if self.rc < 0. and self.rc2 < np.inf:
            warnings.warn('Long range cutoff chosen but will not be applied\
                           for negative inner cutoff values!')

    def todict(self):
        return copy.deepcopy(self._dict)

    def __str__(self):
        return ('Point-charge potential '
                '(points: {}, cutoffs: {:.3f}, {:.3f}, {:.3f} Ang)'
                .format(len(self.q_p),
                        self.rc * Bohr,
                        (self.rc2 - self.width) * Bohr,
                        self.rc2 * Bohr))

    def set_positions(self, R_pv, com_pv=None):
        """Update positions."""
        if com_pv is not None:
            self.com_pv = np.asarray(com_pv) / Bohr
        else:
            self.com_pv = None

        self.R_pv = np.asarray(R_pv) / Bohr
        self.vext_g = None

    def _molecule_distances(self, gd):
        if self.com_pv is not None:
            return self.com_pv - gd.cell_cv.sum(0) / 2

    def calculate_potential(self, gd):
        assert gd.orthogonal
        self.vext_g = gd.zeros()

        dcom_pv = self._molecule_distances(gd)

        _gpaw.pc_potential(gd.beg_c, gd.h_cv.diagonal().copy(),
                           self.q_p, self.R_pv,
                           self.rc, self.rc2, self.width,
                           self.vext_g, dcom_pv)

    def get_forces(self, calc):
        """Calculate forces from QM charge density on point-charges."""
        dens = calc.density
        F_pv = np.zeros_like(self.R_pv)
        gd = dens.finegd
        dcom_pv = self._molecule_distances(gd)

        _gpaw.pc_potential(gd.beg_c, gd.h_cv.diagonal().copy(),
                           self.q_p, self.R_pv,
                           self.rc, self.rc2, self.width,
                           self.vext_g, dcom_pv, dens.rhot_g, F_pv)
        gd.comm.sum(F_pv)
        return F_pv * Ha / Bohr


class CDFTPotential(ExternalPotential):
    # Dummy class to make cDFT compatible with new external
    # potential class ClassName(object):
    def __init__(self, regions, constraints, n_charge_regions,
                 difference):

        self.name = 'CDFTPotential'
        self.regions = regions
        self.constraints = constraints
        self.difference = difference
        self.n_charge_regions = n_charge_regions

    def todict(self):
        return {'name': 'CDFTPotential',
                # 'regions': self.indices_i,
                'constraints': self.v_i * Ha,
                'n_charge_regions': self.n_charge_regions,
                'difference': self.difference,
                'regions': self.regions}


class StepPotentialz(ExternalPotential):
    def __init__(self, zstep, value_left=0, value_right=0):
        """Step potential in z-direction

        zstep: float
            z-value that splits space into left and right [Angstrom]
        value_left: float
            Left side (z < zstep) potentential value [eV]. Default: 0
        value_right: float
            Right side (z >= zstep) potentential value [eV]. Default: 0
       """
        self.value_left = value_left
        self.value_right = value_right
        self.name = 'StepPotentialz'
        self.zstep = zstep

    def __str__(self):
        return 'Step potentialz: {0:.3f} V to {1:.3f} V at z={2}'.format(
            self.value_left, self.value_right, self.zstep)

    def calculate_potential(self, gd):
        r_vg = gd.get_grid_point_coordinates()
        self.vext_g = np.where(r_vg[2] < self.zstep / Bohr,
                               gd.zeros() + self.value_left / Ha,
                               gd.zeros() + self.value_right / Ha)

    def todict(self):
        return {'name': self.name,
                'value_left': self.value_left,
                'value_right': self.value_right,
                'zstep': self.zstep}


class PotentialCollection(ExternalPotential):
    def __init__(self, potentials):
        """Collection of external potentials to be applied

        potentials: list
            List of potentials
        """
        self.potentials = []
        for potential in potentials:
            if isinstance(potential, dict):
                potential = create_external_potential(
                    potential.pop('name'), **potential)
            self.potentials.append(potential)

    def __str__(self):
        text = 'PotentialCollection:\n'
        for pot in self.potentials:
            text += '  ' + pot.__str__() + '\n'
        return text

    def calculate_potential(self, gd):
        self.potentials[0].calculate_potential(gd)
        self.vext_g = self.potentials[0].vext_g.copy()
        for pot in self.potentials[1:]:
            pot.calculate_potential(gd)
            self.vext_g += pot.vext_g

    def todict(self):
        return {'name': 'PotentialCollection',
                'potentials': [pot.todict() for pot in self.potentials]}
