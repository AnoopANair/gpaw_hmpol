"""This module defines different external potentials."""
import copy
import warnings
from typing import Callable, Dict, Optional

import numpy as np
from ase.units import Bohr, Ha

from gpaw.transformers import Transformer

import _gpaw
from gpaw.typing import Array3D
from gpaw.lfc import LFC
from ase.parallel import parprint, paropen

from gpaw.utilities.gauss_octu import Gaussian

import gpaw.point_charge_dist as pcd


__all__ = ['ConstantPotential', 'ConstantElectricField', 'CDFTPotential',
           'PointChargePotential', 'PolarizableEmbedding', 'StepPotentialz',
           'PotentialCollection']

known_potentials: Dict[str, Callable] = {}


def _register_known_potentials():
    from gpaw.bfield import BField
    known_potentials['CDFTPotential'] = lambda: None  # ???
    known_potentials['BField'] = BField
    for name in __all__:
        known_potentials[name] = globals()[name]


def create_external_potential(name, **kwargs):
    """Construct potential from dict."""
    if not known_potentials:
        _register_known_potentials()
    return known_potentials[name](**kwargs)


class ExternalPotential:
    vext_g: Optional[Array3D] = None
    vext_q: Optional[Array3D] = None

    def get_potential(self, gd):
        """Get the potential on a regular 3-d grid.

        Will only call calculate_potential() the first time."""

        if self.vext_g is None:
            self.calculate_potential(gd)
            self.vext_g.flags.writeable = False
        return self.vext_g

    def get_potentialq(self, gd, pd3):
        """Get the potential on a regular 3-d grid in real space.

        Will only call calculate_potential() the first time."""

        if self.vext_q is None:
            vext_g = self.get_potential(gd)
            self.vext_q = pd3.fft(vext_g)
            self.vext_q.flags.writeable = False

        return self.vext_q

    def calculate_potential(self, gd) -> None:
        raise NotImplementedError

    def get_name(self) -> str:
        return self.__class__.__name__

    def update_potential_pw(self, ham, dens) -> float:
        v_q = self.get_potentialq(ham.finegd, ham.pd3).copy()
        eext = ham.pd3.integrate(v_q, dens.rhot_q, global_integral=False)
        dens.map23.add_to1(ham.vt_Q, v_q)
        ham.vt_sG[:] = ham.pd2.ifft(ham.vt_Q)
        if not ham.collinear:
            ham.vt_xG[1:] = 0.0
        return eext

    def update_atomic_hamiltonians_pw(self, ham, W_aL, dens) -> None:
        vext_q = self.get_potentialq(ham.finegd, ham.pd3)
        dens.ghat.integrate(ham.vHt_q + vext_q, W_aL)

    def paw_correction(self, Delta_p, dH_sp) -> None:
        pass

    def derivative_pw(self, ham, ghat_aLv, dens) -> None:
        vext_q = self.get_potentialq(ham.finegd, ham.pd3)
        dens.ghat.derivative(ham.vHt_q + vext_q, ghat_aLv)


class NoExternalPotential(ExternalPotential):
    vext_g = np.zeros((0, 0, 0))

    def update_potential_pw(self, ham, dens) -> float:
        ham.vt_sG[:] = ham.pd2.ifft(ham.vt_Q)
        if not ham.collinear:
            ham.vt_xG[1:] = 0.0
        return 0.0

    def update_atomic_hamiltonians_pw(self, ham, W_aL, dens):
        dens.ghat.integrate(ham.vHt_q, W_aL)

    def derivative_pw(self, ham, ghat_aLv, dens):
        dens.ghat.derivative(ham.vHt_q, ghat_aLv)


class ConstantPotential(ExternalPotential):
    """Constant potential for tests."""
    def __init__(self, constant=1.0):
        self.constant = constant / Ha
        self.name = 'ConstantPotential'

    def __str__(self):
        return 'Constant potential: {:.3f} V'.format(self.constant * Ha)

    def calculate_potential(self, gd):
        self.vext_g = gd.zeros() + self.constant

    def todict(self):
        return {'name': self.name,
                'constant': self.constant * Ha}


class ConstantElectricField(ExternalPotential):
    def __init__(self, strength, direction=[0, 0, 1], tolerance=1e-7):
        """External constant electric field.

        strength: float
            Field strength in V/Ang.
        direction: vector
            Polarisation direction.
        """
        d_v = np.asarray(direction)
        self.field_v = strength * d_v / (d_v**2).sum()**0.5 * Bohr / Ha
        self.tolerance = tolerance
        self.name = 'ConstantElectricField'

    def __str__(self):
        return ('Constant electric field: '
                '({:.3f}, {:.3f}, {:.3f}) V/Ang'
                .format(*(self.field_v * Ha / Bohr)))

    def calculate_potential(self, gd):
        # Currently skipped, PW mode is periodic in all directions
        # d_v = self.field_v / (self.field_v**2).sum()**0.5
        # for axis_v in gd.cell_cv[gd.pbc_c]:
        #     if abs(np.dot(d_v, axis_v)) > self.tolerance:
        #         raise ValueError(
        #             'Field not perpendicular to periodic axis: {}'
        #             .format(axis_v))

        center_v = 0.5 * gd.cell_cv.sum(0)
        r_gv = gd.get_grid_point_coordinates().transpose((1, 2, 3, 0))
        self.vext_g = np.dot(r_gv - center_v, self.field_v)

    def todict(self):
        strength = (self.field_v**2).sum()**0.5
        return {'name': self.name,
                'strength': strength * Ha / Bohr,
                'direction': self.field_v / strength}


class ConstantRSSHPotential(ExternalPotential):
    def __init__(self, strength, center, harmonic, a=59.0):
        """ External constant potential due to a 
            real-space solid harmonic of certain 
            quantum number L (harmonic)

            Depending on the rank the potential
            sets up a field or gradients 
            at the center of zero potential.

            See the index of harmonics here :

        """
        self.strength = strength
        self.center = center
        self.harmonic = int(harmonic)
        self.a = a
        self.name = 'ConstantRSSHPotential'

    def __str__(self):
        return('Constant RSSH potential: %5.5f e/Bohr, harmonic %d' %(self.strength, self.harmonic))

    def calculate_potential(self, gd):

        self.gauss = Gaussian(gd,
                              a=self.a,
                              center=self.center)

        # Need to put together the rank+harmonic
        self.vext_g = self.gauss.get_gauss(self.harmonic) * self.strength
        
    def todict(self):                                        
        return {'name': self.name,                           
                'strength': self.strength,            
                'harmonic': self.harmonic}


class ProductPotential(ExternalPotential):
    def __init__(self, ext_i):
        self.ext_i = ext_i

    def calculate_potential(self, gd):
        self.vext_g = self.ext_i[0].get_potential(gd).copy()
        for ext in self.ext_i[1:]:
            self.vext_g *= ext.get_potential(gd)

    def __str__(self):
        return '\n'.join(['Product of potentials:'] +
                         [ext.__str__() for ext in self.ext_i])

    def todict(self):
        return {'name': self.__class__.__name__,
                'ext_i': [ext.todict() for ext in self.ext_i]}

# Loading in specifically for PE
from gpaw.utilities.timing import Timer
from math import sqrt, pi
         
import _gpaw
from gpaw import debug

import gpaw.mpi as mpi
         
# multipole moments - reduced arrays unique elements
MF = {'d':np.array([0,1,2]),
      'q':np.array([0,1,2,4,5,8]),
      'o':np.array([0,1,2,4,5,8,13,14,17,26]),
      'h':np.array([0,1,2,4,5,8,13,14,17,26,
                    40,41,44,53,80]),
      't':np.array([0,1,2,4,5,8,13,14,17,26,
                    40,41,44,53,80,121,122,
                    125,134,161,243])}
         
# multipole moments - multipliers for unique elements
MM = {'d':np.array([1,1,1], dtype=float),
      'q':np.array([1,2,2,1,2,1], dtype=float),
      'o':np.array([1,3,3,3,6,3,1,3,3,1], dtype=float),
      'h':np.array([1,4,4,6,12,6,4,12,12,4,1,4,6,4,1], dtype=float),
      't':np.array([1,5,5,10,20,10,10,30,30,10,5,20,
                    30,20,5,1,5,10,10,5,1], dtype=float)}

qc_M = np.zeros(9)
qc_M[MF['q']] = MM['q']
oc_M = np.zeros(27)
oc_M[MF['o']] = MM['o']
tc_M = np.zeros(81)
tc_M[MF['h']] = MM['h']

# Coefficients for the real space solid harmonics
# Needs cleaning up - and include the base for charge and dipole
coeffs_rssh = {0 : [sqrt(1 / (4 * pi))
                   ],
               1 : [sqrt(3.0 / (4 * pi)),
                    sqrt(3.0 / (4 * pi)),
                    sqrt(3.0 / (4 * pi))
                   ], 
               2 : [sqrt(15.0 / (4.0 * pi)) / 3.0,
                    sqrt(5.0 /  (16.0 * pi)),
                    sqrt(15.0 / (16.0 * pi)) / 3.0
                   ],
               3 : [sqrt(35.0 / (32.0 * pi))  / 15.0,
                    sqrt(105.0 / (4.0 * pi))  / 15.0,
                    sqrt(21.0 / (32.0 * pi))  / 3.0,
                    sqrt(7.0 / (16.0 * pi))   / 3.0,
                    sqrt(105.0 / (16.0 * pi)) / 15.0
                   ],
               4 : [sqrt(315 / (16.0 * pi)) / 105.0,
                    sqrt(315 / (32.0 * pi)) / 35.0,
                    sqrt(45 / (16.0 * pi)) / 15.0,
                    sqrt(45 / (32.0 * pi)) / 15.0,
                    sqrt(9 / (256.0 * pi)) / 3.0,
                    sqrt(45 / (64.0 * pi)) / 15.0,
                    sqrt(315 / (256.0 * pi)) / 105.0
                   ],
               }

# Generated numerically
coeffs_rssh_1 = {0 : [0.28209479177387814],
                 1 : [0.48860251190291992, 
                      0.48860251190291992, 
                      0.48860251190291992],
                 2 : [0.7283656203947194, 
                      0.7283656203947194, 
                      0.6307831305050401, 
                      0.7283656203947194, 
                      0.3641828101973597],
                 3 : [0.23601743597065739, 
                      1.1562445770562215, 
                      0.9140915989289315, 
                      0.7463526651802308, 
                      0.9140915989289315,
                      0.5781222885281108, 
                      0.23601743597065739],
                 4 : [0.5721926724106754, 
                      0.4046013188068413, 
                      1.5138795132120961, 
                      1.0704744696916628, 
                      0.8462843753216345,
                      1.0704744696916628, 
                      0.7569397566060481, 
                      0.4046013188068413, 
                      0.14304816810266885],
                 }     


coeffs_rssh_2 = {0 : [0.28209479],
                 1 : [0.48860251, 
                      0.48860251, 
                      0.48860251],
                 2 : [0.72836562 / 2.0, 
                      0.72836562 / 2.0, 
                      0.63078313 / 2.0, 
                      0.72836562 / 2.0, 
                      0.36418281 / 2.0],
                 3 : [0.23601744 / 6.0, 
                      1.15624458 / 6.0, 
                      0.9140916  / 6.0, 
                      0.74635267 / 6.0, 
                      0.9140916  / 6.0,
                      0.57812229 / 6.0, 
                      0.23601744 / 6.0],
                 4 : [0.57219267 / 24.0, 
                      0.40460132 / 24.0, 
                      1.51387951 / 24.0, 
                      1.07047447 / 24.0, 
                      0.84628438 / 24.0,
                      1.07047447 / 24.0, 
                      0.75693976 / 24.0, 
                      0.40460132 / 24.0, 
                      0.14304817 / 24.0],
                 }

# Generated numerically                 
coeffs_rssh_3 = {0 : [0.28209479],      
                 1 : [0.48860251,       
                      0.48860251,       
                      0.48860251],      
                 2 : [0.72836562 / 2.0, 
                      0.72836562 / 2.0, 
                      0.63078313 / 2.0, 
                      0.72836562 / 2.0, 
                      0.36418281 / 2.0],
                 3 : [0.23601744 / 2.0, 
                      1.15624458 / 2.0, 
                      0.9140916  / 2.0, 
                      0.74635267 / 2.0, 
                      0.9140916  / 2.0, 
                      0.57812229 / 2.0, 
                      0.23601744 / 2.0],
                 4 : [0.57219267 / 8.0, 
                      0.40460132 / 8.0, 
                      1.51387951 / 8.0, 
                      1.07047447 / 8.0, 
                      0.84628438 / 8.0,
                      1.07047447 / 8.0, 
                      0.75693976 / 8.0, 
                      0.40460132 / 8.0, 
                      0.14304817 / 8.0],
                 }


class PolarizableEmbedding(ExternalPotential):

    def __init__(self, mm, qm, mp, calcmm, lrc=11.0, width=2.0, 
                 damping_value=0.75,
                 grid_mask=None, Nc=np.array([0,0,0]), 
                 Nc_outer=np.array([0,0,0]), **parameters):
        """ Class to handle external potential due to polarizable
            multipoles from SCME type calculator object.
        """
        self._dict = dict(name=self.__class__.__name__,
                          mm=mm, qm=qm, mp=mp, calcmm=calcmm,
                          damping_value=damping_value,
                          grid_mask=grid_mask, Nc=Nc)

        self.mm     = mm      # mm atoms object
        self.mp     = mp      # no. atoms per center of mass in MM
        # EOJ : calcmm is now Anoops new cpp SCME code
        self.calcmm = calcmm  # mm calculator (SCME)
        self.qm     = qm      # qm atoms object
        #
        self.qmidx  = len(qm) # no. QM atoms
        #
        self.nm     = len(self.mm) // self.mp # No. MM molecules
        self.timer  = Timer()

        self.damping_value = damping_value

        # Long range cut-off in Bohr
        self.lrc = lrc / Bohr
        self.width = width / Bohr

        # Near-neighbor images
        self.Nc = Nc
        self.Nc_outer = Nc_outer

        # Make NN grid
        self.NNc = self.make_grid(self.Nc)

        # MISSING ASSERTIONS THAT Nc and Nc_outer make sense!

        # Need this for the external potential, for now
        if (self.Nc_outer > 0).any():
            self.NT        = self.make_grid(self.Nc_outer)
            self.NNc_outer = self.make_grid(self.Nc_outer, self.NNc)
        else:
            self.NT        = self.make_grid(self.Nc)
            self.NNc_outer = np.array([np.zeros(3)])

        # For PBC we need to pass 1/0 and cell dim in Bohr
        self.pbc = np.ones(3) * self.qm.pbc
        self.Rc  = self.qm.cell.diagonal() / Bohr

        # Hold on to old arrays
        self.dpoles = None
        self.qpoles = None
        self.dpoles_1 = None
        self.qpoles_1 = None

        # Hold on to higher order potential (octu- and hexadecapole)
        self.potential_oh = None

        #
        self.vext_g = None
        self.dummy  = False

        # pseudo core charge on coarse grid
        self.ghat_cg = None

        # Explicit gaussian charge densities
        self.gauss = None

        self.initial = True

    def make_grid(self, Nc, cmpr=None):
        """ Make near-neighbor cell grids
        """
        NN = []
        g = np.zeros(3)

        for i in range(-Nc[0],Nc[0]+1,1):
            for j in range(-Nc[1],Nc[1]+1,1):
                for k in range(-Nc[2],Nc[2]+1,1):
                    g[0] = i
                    g[1] = j
                    g[2] = k

                    if cmpr is not None:
                       if ~np.array([(g == nn).all() for i, nn in enumerate(cmpr)]).any():
                           NN.append(g)
                    else:
                       NN.append(g)
                    g = np.zeros(3)
        return np.array(NN, dtype=int)

    # get_potential supersedes ExternalPotential / need PW equivalent...
    def get_potential(self, gd=None, density=None,
                      setups=None, nspins=None, forces=False):
        """ Create external potential from dipoles
            up to and including hexadecapole with 
            origin at the center of mass of each 
            SCME water molecule in the MM atoms object
        """

        if self.initial:
            self.QM_cell_mid = 0.5 * density.gd.cell_cv.sum(0)
            # Update dynamic part of potential
            self.update_potential(gd=gd, density=density,
                                  setups=setups, nspins=nspins)

            self.vext_g = self.potential.copy()
            # Add static part of potential
            self.vext_g += self.potential_oh

            return self.vext_g

        elif not self.dummy:
            # EOJ : This 'dummy' variable needs to change.
            if not self.check_convergence(density=density):
                self.update_potential(gd=gd, 
                                      density=density,
                                      setups=setups, 
                                      nspins=nspins)
            else:  # when converged update the non-induced Eoh
                # one more scme calc with inscf=False -- WHY???
                cc = self.qm.calc.parameters['convergence']['dpoles']
                self.calcmm.calculate(self.mm, 
                                      denserror=cc, 
                                      inscf=False,
                                      converged=True)
                

            self.vext_g = self.potential.copy()
            # Add static part of potential
            self.vext_g += self.potential_oh

            if forces:
                # Eval all electric field terms with conv. density
                gradients = self.get_efield(density, 
                                            setups, 
                                            nspins, 
                                            forces=forces)
                #####################################
                #self.update_potential(gd=gd,
                #                      density=density,
                #                      setups=setups,
                #                      nspins=nspins)

                # Pass values to SCME
                " --> Needs send-receive "
                mm = self.mm
                self.calcmm.update_qm_gradients(gradients)

                # Make sure SCME uses the fields and derivatives
                # EOJ :: Again, lets remove all D3 nonsense
                cc = self.qm.calc.parameters['convergence']['dpoles']
                self.calcmm.calculate(mm, 
                                      denserror=cc, 
                                      inscf=False,
                                      converged=True)

            return self.vext_g
        
        else:
            if hasattr(self, 'potential'):
                if gd == self.gd or gd is None:
                # Nothing changed
                    return self.vext_g

    def update_potential(self, gd=None, density=None,
                         setups=None, nspins=None):
 
        # Make sure old arrays are used
        if gd is not None:
            self.gd = gd 
                         
        if density is not None:
            self.density = density

        if self.cm_gc is None:
            self.cm_gc = self._molecule_distances(self.cm, self.gd)

        # Save old dipoles (for induction convergence check)
        if self.dpoles is not None:
            self.dpoles_1 = self.dpoles.copy()
            self.qpoles_1  = self.qpoles.copy()
                         
        # Grab electric field and derivative values
        self.timer.start('Electric Field and Gradients')
        gradients = self.get_efield(self.density, 
                                         setups, 
                                         nspins)
        self.timer.stop('Electric Field and Gradients')
                         
        calcmm = self.calcmm
        mm = self.mm     
                         
        #####################################
        # Pass values to SCME
        self.calcmm.update_qm_gradients(gradients)
        self.timer.start('SCME SCF Loop')
        calcmm.calculate(mm, 
                         denserror=self.density.error, 
                         inscf=True,
                         converged=False)
        self.timer.stop('SCME SCF Loop')
        #####################################
                         
        # Values are in atomic-units
        moments = calcmm.get_moments()

        ### PLACEHOLDER FOR FUTURE COARSE GRAINING OF MM REGION                
        ### EOJ : COARSE SCME SPACE ALONG NON-PERIODIC DIMENSION
        # dpoles-slices
        # qpoles-slices
        # opoles-slices
        # hpoles-slices
        # POSITION OF THE SLICES - COMs

        # EOJ : Hack until we figure out robust vext(rG)
        # Suppress coarse-grid stuff
        Nc_outer = self.Nc_outer.copy()
        self.Nc_outer = np.zeros(3)

        NNc = self.NNc.copy()
        self.NNc = self.NT
        # ... for now
        
        potential = np.ascontiguousarray(np.zeros(gd.end_c-gd.beg_c))
        cgd = self.density.gd
        self.timer.start('Evaluate Potential')
        # Check octo / or make empty potential -- potential by default made on
        # finegd - only make it with the finegd for masked cm's

        # EOJ: Similar to efield we collect grid spaces, masks, and potentials
        # into arrays [gd], [mask], [pot], [pot_oh], [interpolator]
        # gd = [self.gd]
        # masks = [mask]
        # pot = [np.ascontiguousarray(np.zeros(gd.end_c-gd.beg_c))]
        # pot_oh = [np.ascontiguousarray(np.zeros(gd.end_c-gd.beg_c))]

        dpoles = moments[0]
        qpoles = moments[1]
        opoles = moments[2]
        hpoles = moments[3]

        mask = self.grid_mask

        if self.potential_oh is None:
            self.potential_oh = np.ascontiguousarray(np.zeros(gd.end_c-gd.beg_c))
            _gpaw.multipole_potential(gd.beg_c, 
                                      np.ascontiguousarray(gd.h_cv.diagonal().copy()),
                                      dpoles[mask], qpoles[mask], self.cm[mask],
                                      self.cm_gc[mask],
                                      potential, self.NNc, self.lrc, self.width, 
                                      self.damping_value, self.pbc, self.Rc,
                                      self.potential_oh, opoles[mask], hpoles[mask],
                                      MM['o'], tc_M)
            # EOJ : fix MM['o'] vs tc_M; use either one.
        else:            
            _gpaw.multipole_potential(gd.beg_c, 
                                      np.ascontiguousarray(gd.h_cv.diagonal().copy()),
                                      dpoles[mask], qpoles[mask], self.cm[mask],
                                      self.cm_gc[mask],
                                      potential, self.NNc,
                                      self.lrc, self.width,
                                      self.damping_value, self.pbc, self.Rc)
        
        # Most NNs are treated on a coarse grid
        if (self.Nc_outer > 0).any():
            potential_nn_cg = np.ascontiguousarray(np.zeros(cgd.end_c-cgd.beg_c))
            if self.initial:   
                potential_nn_coh = np.ascontiguousarray(np.zeros(cgd.end_c-cgd.beg_c))
                _gpaw.multipole_potential(cgd.beg_c, 
                                      np.ascontiguousarray(cgd.h_cv.diagonal().copy()),
                                      dpoles[mask], qpoles[mask], self.cm[mask],
                                      self.cm_gc[mask],
                                      potential_nn_cg, self.NNc_outer,
                                      self.lrc, self.width, 
                                      self.damping_value, self.pbc, self.Rc,
                                      potential_nn_coh, opoles[mask], hpoles[mask],
                                      MM['o'], tc_M)
            else:                                   
                _gpaw.multipole_potential(cgd.beg_c,          
                                      np.ascontiguousarray(cgd.h_cv.diagonal().copy()),
                                      dpoles[mask], qpoles[mask], self.cm[mask],
                                      self.cm_gc[mask],      
                                      potential_nn_cg, self.NNc_outer,  
                                      self.lrc, self.width,   
                                      self.damping_value, self.pbc, self.Rc)

        self.timer.stop('Evaluate Potential')

        if (self.Nc_outer > 0).any():
             interpolate = Transformer(density.gd, density.finegd, 4)
             if self.initial:           
                 f_potential_nn_oh = density.finegd.empty()
                 f_potential_nn = density.finegd.empty()
                 interpolate.apply(potential_nn_coh, f_potential_nn_oh)
                 interpolate.apply(potential_nn_cg, f_potential_nn)
                 potential += f_potential_nn          
                 self.potential_oh += f_potential_nn_oh 
             else:                      
                 f_potential_nn = density.finegd.empty()         
                 interpolate.apply(potential_nn_cg, f_potential_nn)
                 potential += f_potential_nn

        #
        self.Nc_outer = Nc_outer.copy()
        self.NNc = NNc.copy()

        # Potential updated
        self.initial = False
                         
        self.potential = potential.copy()

        # Hold on to
        # EOJ: ADD function update_step(gd, d1v, d2v, dpoles, qpoles)
        self.gd = gd
        self.qpoles = qpoles.copy()
        self.dpoles = dpoles.copy()

    def cubic_correction(self, CGobj, FGobj):
        dC = CG_obj - (FGobj[::2,::2,::2]   + FGobj[1::2,::2,::2] + 
                       FGobj[::2,1::2,::2]  + FGobj[::2,::2,1::2] + 
                       FGobj[1::2,1::2,::2] + FGobj[::2,1::2,1::2] +
                       FGobj[1::2,::2,1::2] + FGobj[1::2,1::2,1::2])
        # EOJ : Feb/23
        return CG_obj + dC                  

    def check_convergence(self, density=None): 
        if density != None:                  
            self.density = density           
                                             
        convcrit = self.qm.calc.parameters['convergence']['dpoles']
        convcritq = self.qm.calc.parameters['convergence']['qpoles']
                                             
        if self.dpoles_1 is not None:        
            maxdip, maxqua = self.check_difference()                     
            converged = (maxdip < convcrit) and (maxqua < convcritq)     
                                             
            return converged                 
        else:                                
            return False

    def origin_shift(self, 
                     cm,
                     moments,
                     density,
                     a=59.,
                     dC=None,
                     center=None,
                     xyz=[1,1,1]):

        """ Utility tool which grabs cartesian moments,
            expands them as RSSH functions on a grid
            and calculates moments at a single center.
                 
            cm      :: positions of centers in Bohr
            moments :: dipoles, qpoles, opoles, hpoles
                       ass with each cm
            center  :: position of the single center
            xyz     :: utility integers to anchor
                       the center to certain PBCs
                 
            IF no center is included the center of the grid 
            space is used.

            All of the moments are added at once and 
            a real-space integration calculates the central
            moment.
                 
        """     
        # Do this on the coarse grid
        fg = density.finegd
        gd = fg.coarsen()
        rhot_G = gd.zeros()

        # Detect boundary
        for i, center in enumerate(cm):
            coeffs = self.cartesian_to_rssh([moments[0][i], 
                                             moments[1][i], 
                                             moments[2][i], 
                                             moments[3][i]])
            for j in [1, 2, 3 ,4]:
                self.add_rssh(gd, rhot_G, j, coeffs[j], center=center)

        # Given all moments on grid integrate at cell center
        D_v = np.zeros(3)
        Q_v = np.zeros((3,3))
        O_v = np.zeros((3,3,3))
        H_v = np.zeros((3,3,3,3))
 
        _gpaw.calculate_moments(gd.beg_c,
                                gd.h_cv.diagonal().copy(),
                                self.QM_cell_mid,
                                rhot_G,
                                D_v, Q_v, O_v, H_v,
                                self.Rc, self.pbc)
 
        gd.comm.sum(D_v)
        gd.comm.sum(Q_v)
        gd.comm.sum(O_v)
        gd.comm.sum(H_v)

        return [D_v, Q_v, O_v, H_v]

    def add_rssh(self, gd, rhot_G, rank, coeffs, center=None):
        """ Add explicit charge-like densities described 
            with real solid spherical harmonic
        """

        if center is None:
            center=self.QM_cell_mid

        gauss = Gaussian(gd,
                         a=59.,
                         center=center)

        if rank == 0:
            rhot_G += gauss.get_gauss(0) * coeffs_rssh[0][0] * coeffs[0]
        elif rank == 1:
            rhot_G += gauss.get_gauss(1) * coeffs_rssh[1][0] * coeffs[0]
            rhot_G += gauss.get_gauss(2) * coeffs_rssh[1][1] * coeffs[1]
            rhot_G += gauss.get_gauss(3) * coeffs_rssh[1][2] * coeffs[2]
        elif rank == 2:
            rhot_G += gauss.get_gauss(4) * coeffs_rssh[2][0] * coeffs[0]
            rhot_G += gauss.get_gauss(5) * coeffs_rssh[2][0] * coeffs[1]
            rhot_G += gauss.get_gauss(6) * coeffs_rssh[2][1] * coeffs[2]
            rhot_G += gauss.get_gauss(7) * coeffs_rssh[2][0] * coeffs[3]
            rhot_G += gauss.get_gauss(8) * coeffs_rssh[2][2] * coeffs[4]
        elif rank == 3:
            rhot_G += gauss.get_gauss(9)  * coeffs_rssh[3][0] * coeffs[0]
            rhot_G += gauss.get_gauss(10) * coeffs_rssh[3][1] * coeffs[1]
            rhot_G += gauss.get_gauss(11) * coeffs_rssh[3][2] * coeffs[2]
            rhot_G += gauss.get_gauss(12) * coeffs_rssh[3][3] * coeffs[3]
            rhot_G += gauss.get_gauss(13) * coeffs_rssh[3][2] * coeffs[4]
            rhot_G += gauss.get_gauss(14) * coeffs_rssh[3][4] * coeffs[5]
            rhot_G += gauss.get_gauss(15) * coeffs_rssh[3][0] * coeffs[6]
        elif rank == 4:
            rhot_G += gauss.get_gauss(16) * coeffs_rssh[4][0] * coeffs[0]
            rhot_G += gauss.get_gauss(17) * coeffs_rssh[4][1] * coeffs[1]
            rhot_G += gauss.get_gauss(18) * coeffs_rssh[4][2] * coeffs[2]
            rhot_G += gauss.get_gauss(19) * coeffs_rssh[4][3] * coeffs[3]
            rhot_G += gauss.get_gauss(20) * coeffs_rssh[4][4] * coeffs[4]
            rhot_G += gauss.get_gauss(21) * coeffs_rssh[4][3] * coeffs[5]
            rhot_G += gauss.get_gauss(22) * coeffs_rssh[4][5] * coeffs[6]
            rhot_G += gauss.get_gauss(23) * coeffs_rssh[4][1] * coeffs[7]
            rhot_G += gauss.get_gauss(24) * coeffs_rssh[4][6] * coeffs[8]

    def evaluate_moments(self, density):
        # moments, rank 1-4
        D_v = np.zeros(3)
        Q_v = np.zeros((3,3))
        O_v = np.zeros((3,3,3))
        H_v = np.zeros((3,3,3,3))

        _gpaw.calculate_moments(density.finegd.beg_c, 
                                density.finegd.h_cv.diagonal().copy(),
                                self.QM_cell_mid, 
                                density.rhot_g, 
                                D_v, Q_v, O_v, H_v,
                                self.Rc, self.pbc)

        density.finegd.comm.sum(D_v)
        density.finegd.comm.sum(Q_v)
        density.finegd.comm.sum(O_v)
        density.finegd.comm.sum(H_v)

        # EOJ: Feb/23 Ctrl for debuggin, remove when done
        D_v *= 1.0
        Q_v *= 1.0
        O_v *= 1.0
        H_v *= 1.0

        # EOJ : Feb/23 
        # We take a copy of the QM cartesian moments
        self.QM_d_v = D_v.copy() 
        self.QM_q_v = Q_v.copy()
        self.QM_o_v = O_v.copy()
        self.QM_h_v = H_v.copy()

        # Arrange according to Roastgaards table:
        coeffs = self.cartesian_to_rssh([self.QM_d_v,
                                         self.QM_q_v,
                                         self.QM_o_v,
                                         self.QM_h_v])

        return coeffs

    def cartesian_to_rssh(self, moments):
        # Transform cartesian moments to RSSH 
        # according to Roastgaards table
        D_v = moments[0]
        Q_v = moments[1]
        O_v = moments[2]
        H_v = moments[3]

        coeffs = {0 : [0,0
                     ],
                  1 : [D_v[1],
                       D_v[2],
                       D_v[0],
                     ],
                  2 : [Q_v[0,1],
                       Q_v[1,2],
                       Q_v[2,2],
                       Q_v[0,2],
                       Q_v[0,0] - Q_v[1,1]
                     ],
                  3 : [3.0 * O_v[0,0,1] - O_v[1,1,1],
                       O_v[0,1,2],
                       O_v[1,2,2],
                       O_v[2,2,2],
                       O_v[0,2,2],
                       O_v[0,0,2] - O_v[1,1,2],
                       O_v[0,0,0] - 3.0 * O_v[0,1,1]
                     ],
                  4 : [H_v[0,0,0,1] - H_v[0,1,1,1],
                       3.0 * H_v[0,0,1,2] - H_v[1,1,1,2],
                       H_v[0,1,2,2],
                       H_v[1,2,2,2],
                       H_v[2,2,2,2],
                       H_v[0,2,2,2],
                       H_v[0,0,2,2] - H_v[1,1,2,2],
                       H_v[0,0,0,2] - 3.0 * H_v[0,1,1,2],
                       H_v[0,0,0,0] + H_v[1,1,1,1] - 6.0 * H_v[0,0,1,1]
                     ],
                 }

        return coeffs

    def rssh_expansion_density(self, density):
        #
        fg = density.finegd
        gd = fg.coarsen()
        rhot_G = gd.zeros()

        coeffs = self.evaluate_moments(density)

        for i in [1,2,3,4]:
            self.add_rssh(gd, rhot_G, i, coeffs[i])

        return rhot_G

    def check_difference(self):              
        if self.dpoles_1 is not None:        
            dip = abs(self.dpoles - self.dpoles_1 + 1e-12).sum(axis=1)   
                                             
            normq = np.array([np.linalg.norm(q) for q in self.qpoles])   
            normq_1 = np.array([np.linalg.norm(q) for q in self.qpoles_1])
            qua = abs(normq - normq_1 + 1e-12)
                                               
            return np.max([dip]), np.max([qua]) 
        else:                                  
            return np.nan, np.nan  # first round

    # EOJ : Feb/23 - Calculate electric field and gradients due to ME of QM
    #def get_efield_from_moments(self, density, d1v, d2v, d3v, d4v, d5v, forces=False):
    #    """ Evaluate electric field and gradients at each
    #        MM center of mass from a multipole expansion of
    #        the QM system and add to the input arrays.
    #    """
    #    pass


    def get_efield(self, density, setups, nspins, forces=False):
        """ Evaluate electric field at each cm
            from total psuedo charge density - on fine or coarse grid
        """      
        gd = self.gd
        #        
        d1v = np.ascontiguousarray(np.zeros((self.nm,3)))
        d2v = np.ascontiguousarray(np.zeros((self.nm,3,3)))
        d3v = np.ascontiguousarray(np.zeros((self.nm,3,3,3)))
        d4v = np.ascontiguousarray(np.zeros((self.nm,3,3,3,3)))
        d5v = np.ascontiguousarray(np.zeros((self.nm,3,3,3,3,3)))
                 
        mask = self.grid_mask

        # Lists/[] hold the different splicings and charge densities,
        # Call function evaluate_efield with entries in []
        # i.e. [mask], [grids], [densities]
        m = [mask]
        g = [density.finegd]
        r = [density.rhot_g]
        N = [self.NNc]
        d1 = [d1v[mask].copy()]
        d2 = [d2v[mask].copy()]
        d3 = [d3v[mask].copy()]
        d4 = [d4v[mask].copy()]
        d5 = [d5v[mask].copy()]

        # Outside of lrc or in near-neighbor cell      
        if (self.Nc_outer > 0).any():
            cgd = density.finegd.coarsen()
            rhot_G = self.rssh_expansion_density(density)
            #

        # NN        
        if (self.Nc_outer > 0).any():
            m.append(mask)
            g.append(cgd)
            r.append(rhot_G)
            N.append(self.NNc_outer)
            d1.append(d1v[mask].copy())
            d2.append(d2v[mask].copy())
            d3.append(d3v[mask].copy())
            d4.append(d4v[mask].copy())
            d5.append(d5v[mask].copy())

        # EOJ: ONE function which loops through m
        # EOJ : Feb/23, modified such that d1v and d2v are evaluated using cartesian
        # QM moments, anchored to the center of the cell
        if forces:
            for i, msk in enumerate(m):
                _gpaw.electric_field_com(g[i].beg_c, 
                                    np.ascontiguousarray(g[i].h_cv.diagonal().copy()),
                                    d1[i], d2[i], self.cm[msk],
                                    self.cm_gc[msk],
                                    r[i],
                                    N[i],
                                    self.lrc, self.width,
                                    self.damping_value, self.pbc, self.Rc,
                                    d3[i], d4[i], d5[i])

                # Collect over domains
                g[i].comm.sum(d1[i])
                g[i].comm.sum(d2[i])
                g[i].comm.sum(d3[i])
                g[i].comm.sum(d4[i])
                g[i].comm.sum(d5[i])
                     
                d1v[msk] += d1[i]
                d2v[msk] += d2[i]
                d3v[msk] += d3[i]
                d4v[msk] += d4[i]
                d5v[msk] += d5[i]

        else:
            # Grid integration
            _gpaw.electric_field_com(g[0].beg_c,   
                                   np.ascontiguousarray(g[0].h_cv.diagonal().copy()),
                                   d1[0], d2[0], self.cm[m[0]],
                                   self.cm_gc[m[0]],
                                   r[0],
                                   N[0],         
                                   self.lrc, self.width,
                                   self.damping_value, self.pbc, self.Rc)

            # Collect over domains
            g[0].comm.sum(d1[0])
            g[0].comm.sum(d2[0])
            d1v[m[0]] += d1[0]
            d2v[m[0]] += d2[0]

            # Single QM center moments
            # EOJ : 13Mar - these can be effectively swithed out for the 
            # analytical Gaussian functions
            if (self.Nc_outer > 0).any():
                d1vQMc, d2vQMc = self.get_efield_from_moments(density)
                d1v[m[0]] += d1vQMc
                d2v[m[0]] += d2vQMc

        self.d1v = d1v 
        self.d2v = d2v
        self.d3v = d3v
        self.d4v = d4v
        self.d5v = d5v

        return d1v, d2v, d3v, d4v, d5v

    # EOJ : 13Mar
    #def get_analytical_fields_from_moments(self, density):
    #    _ = self.evaluate_moments(density)
    #    pass

    def get_efield_from_moments(self, density):
        # Update moments - already calculated because of call to rssh_expansion_density
        # _ = self.evaluate_moments(density)

        # Test d1v and d2v versus RSSH
        d1v = np.ascontiguousarray(np.zeros((self.nm,3)))
        d2v = np.ascontiguousarray(np.zeros((self.nm,3,3)))

        _gpaw.electric_field_expansion(self.cm,
                                       self.QM_cell_mid,
                                       self.Rc, 
                                       self.NNc_outer,
                                       d1v,
                                       d2v,
                                       self.QM_d_v,
                                       self.QM_q_v,
                                       self.QM_o_v,
                                       self.QM_h_v,
                                       qc_M,
                                       oc_M,
                                       tc_M,
                                       self.pbc) # Unnecessary at the moment

        return d1v, d2v

    def update_atoms(self, mmatoms=None, qmatoms=None, mmcalc=None, grid_mask=None):
        """ Called from ase.calculator.qmmm - updates to the total system
            requires reset of everything here ... Grid mask functionality
            is in ase.calculator.qmmm...
        """
        self.mm = None
        self.qm = None
        self.calcmm = None
        self.dpoles_1 = None
        self.grid_mask = None
        #        
        self.mm = mmatoms
        self.qm = qmatoms
        self.calcmm = mmcalc
        self.cm = self.get_cm(self.nm)

        self.grid_mask = grid_mask
                 
        self.initial = True
        self.vext_g  = None
        self.potential_oh = None
        self.cm_gc = None

    def todict(self):             
        return self._dict
                                  
    def __str__(self):            
        return ('Polarizable embedding potential '
                '(sites: {}, damping: {:.3f} Ang**-1)'
                .format(self.nm,
                        self.damping_value))

    def get_cm(self, n):
        """ Return center of mass in units Bohr
        """
        cm   = np.zeros((int(n), 3))
        atoms = self.mm
        mp = self.mp
     
        for i in range(int(n)):
            cm[i,:] += atoms[i*mp:(i+1)*mp].get_center_of_mass() / Bohr
        return cm

    def _molecule_distances(self, cm, gd):
        """ Return MM com distance to the center of the QM cell
        """
        return cm - gd.cell_cv.sum(0) / 2


class PointChargePotential(ExternalPotential):
    def __init__(self, charges, positions=None,
                 rc=0.2, rc2=np.inf, width=1.0):
        """Point-charge potential.

        charges: list of float
            Charges.
        positions: (N, 3)-shaped array-like of float
            Positions of charges in Angstrom.  Can be set later.
        rc: float
            Inner cutoff for Coulomb potential in Angstrom.
        rc2: float
            Outer cutoff for Coulomb potential in Angstrom.
        width: float
            Width for cutoff function for Coulomb part.

        For r < rc, 1 / r is replaced by a third order polynomial in r^2 that
        has matching value, first derivative, second derivative and integral.

        For rc2 - width < r < rc2, 1 / r is multiplied by a smooth cutoff
        function (a third order polynomium in r).

        You can also give rc a negative value.  In that case, this formula
        is used::

            (r^4 - rc^4) / (r^5 - |rc|^5)

        for all values of r - no cutoff at rc2!
        """
        self._dict = dict(name=self.__class__.__name__,
                          charges=charges, positions=positions,
                          rc=rc, rc2=rc2, width=width)
        self.q_p = np.ascontiguousarray(charges, float)
        self.rc = rc / Bohr
        self.rc2 = rc2 / Bohr
        self.width = width / Bohr
        if positions is not None:
            self.set_positions(positions)
        else:
            self.R_pv = None

        if abs(self.q_p).max() < 1e-14:
            warnings.warn('No charges!')
        if self.rc < 0. and self.rc2 < np.inf:
            warnings.warn('Long range cutoff chosen but will not be applied\
                           for negative inner cutoff values!')

    def todict(self):
        return copy.deepcopy(self._dict)

    def __str__(self):
        return ('Point-charge potential '
                '(points: {}, cutoffs: {:.3f}, {:.3f}, {:.3f} Ang)'
                .format(len(self.q_p),
                        self.rc * Bohr,
                        (self.rc2 - self.width) * Bohr,
                        self.rc2 * Bohr))

    def set_positions(self, R_pv, com_pv=None):
        """Update positions."""
        if com_pv is not None:
            self.com_pv = np.asarray(com_pv) / Bohr
        else:
            self.com_pv = None

        self.R_pv = np.asarray(R_pv) / Bohr
        self.vext_g = None

    def _molecule_distances(self, gd):
        if self.com_pv is not None:
            return self.com_pv - gd.cell_cv.sum(0) / 2

    def calculate_potential(self, gd):
        assert gd.orthogonal
        self.vext_g = gd.zeros()

        dcom_pv = self._molecule_distances(gd)

        _gpaw.pc_potential(gd.beg_c, gd.h_cv.diagonal().copy(),
                           self.q_p, self.R_pv,
                           self.rc, self.rc2, self.width,
                           self.vext_g, dcom_pv)

    def get_forces(self, calc):
        """Calculate forces from QM charge density on point-charges."""
        dens = calc.density
        F_pv = np.zeros_like(self.R_pv)
        gd = dens.finegd
        dcom_pv = self._molecule_distances(gd)

        _gpaw.pc_potential(gd.beg_c, gd.h_cv.diagonal().copy(),
                           self.q_p, self.R_pv,
                           self.rc, self.rc2, self.width,
                           self.vext_g, dcom_pv, dens.rhot_g, F_pv)
        gd.comm.sum(F_pv)
        return F_pv * Ha / Bohr


class CDFTPotential(ExternalPotential):
    # Dummy class to make cDFT compatible with new external
    # potential class ClassName(object):
    def __init__(self, regions, constraints, n_charge_regions,
                 difference):

        self.name = 'CDFTPotential'
        self.regions = regions
        self.constraints = constraints
        self.difference = difference
        self.n_charge_regions = n_charge_regions

    def todict(self):
        return {'name': 'CDFTPotential',
                # 'regions': self.indices_i,
                'constraints': self.v_i * Ha,
                'n_charge_regions': self.n_charge_regions,
                'difference': self.difference,
                'regions': self.regions}


class StepPotentialz(ExternalPotential):
    def __init__(self, zstep, value_left=0, value_right=0):
        """Step potential in z-direction

        zstep: float
            z-value that splits space into left and right [Angstrom]
        value_left: float
            Left side (z < zstep) potentential value [eV]. Default: 0
        value_right: float
            Right side (z >= zstep) potentential value [eV]. Default: 0
       """
        self.value_left = value_left
        self.value_right = value_right
        self.name = 'StepPotentialz'
        self.zstep = zstep

    def __str__(self):
        return 'Step potentialz: {0:.3f} V to {1:.3f} V at z={2}'.format(
            self.value_left, self.value_right, self.zstep)

    def calculate_potential(self, gd):
        r_vg = gd.get_grid_point_coordinates()
        self.vext_g = np.where(r_vg[2] < self.zstep / Bohr,
                               gd.zeros() + self.value_left / Ha,
                               gd.zeros() + self.value_right / Ha)

    def todict(self):
        return {'name': self.name,
                'value_left': self.value_left,
                'value_right': self.value_right,
                'zstep': self.zstep}


class PotentialCollection(ExternalPotential):
    def __init__(self, potentials):
        """Collection of external potentials to be applied

        potentials: list
            List of potentials
        """
        self.potentials = []
        for potential in potentials:
            if isinstance(potential, dict):
                potential = create_external_potential(
                    potential.pop('name'), **potential)
            self.potentials.append(potential)

    def __str__(self):
        text = 'PotentialCollection:\n'
        for pot in self.potentials:
            text += '  ' + pot.__str__() + '\n'
        return text

    def calculate_potential(self, gd):
        self.potentials[0].calculate_potential(gd)
        self.vext_g = self.potentials[0].vext_g.copy()
        for pot in self.potentials[1:]:
            pot.calculate_potential(gd)
            self.vext_g += pot.vext_g

    def todict(self):
        return {'name': 'PotentialCollection',
                'potentials': [pot.todict() for pot in self.potentials]}

def static_polarizability(atoms, strength=0.01):
    """Calculate polarizability tensor
    
    atoms: Atoms object
    strength: field strength in V/Ang
    
    Returns
    -------
    polarizability tensor:
        Unit (e^2 Angstrom^2 / eV).
        Multiply with Bohr * Ha to get (Angstrom^3)
    """
    atoms.get_potential_energy()
    calc = atoms.calc
    assert calc.parameters.external is None
    dipole_gs = calc.get_dipole_moment()
    
    alpha = np.zeros((3, 3))
    for c in range(3):
        axes = np.zeros(3)
        axes[c] = 1
        calc.set(external=ConstantElectricField(strength, axes))
        calc.get_potential_energy()
        alpha[c] = (calc.get_dipole_moment() - dipole_gs) / strength
    calc.set(external=None)
    
    return alpha.T

def polarizability(atoms, strength):
    E0 = atoms.get_potential_energy() / Ha

    calc = atoms.calc
    
    F = strength

    # a_xx
    calc.set(external=ConstantElectricField(F, np.array([0,0,1])))
    EF = calc.get_potential_energy() / Ha

    calc.set(external=ConstantElectricField(-F, np.array([0,0,1])))
    EF_ = calc.get_potential_energy() / Ha

    calc.set(external=ConstantElectricField(2*F, np.array([0,0,1])))
    E2F = calc.get_potential_energy() / Ha

    calc.set(external=ConstantElectricField(-2*F, np.array([0,0,1])))
    E2F_ = calc.get_potential_energy() / Ha


    F *= Bohr / Ha
    a_zz = (5. / 2.0 * E0 - 4. / 3. * (EF + EF_) + 1. / 12. * (E2F + EF_)) / (F**2)
    return a_zz




def apply_field_gradient(atoms, 
                         shift,
                         field_grad_strength, 
                         field_grad_direction):

    """Apply field gradient using point charges

    atoms: Atoms object
    strength: field strength in Ha/Bohr

    Returns
    -------
    polarizability tensor:
        Unit (e^2 Bohr^3).
    """
    shift = shift/Bohr
    COM = atoms.get_center_of_mass() 
    cell = atoms.cell.diagonal() 
    cell_center = cell / 2.
    dc = cell_center - COM
    pos = atoms.get_positions().copy()
    atoms.set_positions(pos + (dc+shift))

    # dRx, dRy, dRz
    dRx = cell_center[0] / 2.0 + 10.0 + shift[0]
    dRy = cell_center[1] / 2.0 + 10.0 + shift[1]
    dRz = cell_center[2] / 2.0 + 10.0 + shift[2]

    dRmax = max(dRx,dRy,dRz) + 5


    chargepostions, charges = pcd.Vij(  (cell/2+shift)/Bohr,
                                        dRmax/Bohr,
                                        field_grad_strength,
                                        field_grad_direction)

    calc = atoms.calc
    external= PointChargePotential(charges, chargepostions*Bohr)
    calc.set(external=external)
    atoms.set_calculator(calc)

    E = atoms.get_potential_energy()
    return E




def apply_field(atoms, 
                shift, 
                field_strength, 
                field_direction):
    
    """Apply field using point charges

    atoms: Atoms object
    strength: field strength in Ha/Bohr

    Returns
    -------
    polarizability tensor:
        Unit (e^2 Bohr^3).
    """

    shift = shift/Bohr
    COM = atoms.get_center_of_mass() 
    cell = atoms.cell.diagonal() 
    cell_center = cell / 2.
    dc = cell_center - COM
    pos = atoms.get_positions().copy()
    atoms.set_positions(pos + (dc+shift))

    # dRx, dRy, dRz
    dRx = cell_center[0] / 2.0 + 10.0 + shift[0]
    dRy = cell_center[1] / 2.0 + 10.0 + shift[1]
    dRz = cell_center[2] / 2.0 + 10.0 + shift[2]

    dRmax = max(dRx,dRy,dRz)
    dRmax = dRmax + 5

    chargepostions, charges = pcd.Vi(   (cell/2+shift)/Bohr,
                                        dRmax/Bohr,
                                        field_strength,
                                        field_direction)

    calc = atoms.calc
    external= PointChargePotential(charges, chargepostions*Bohr)
    calc.set(external=external)
    atoms.set_calculator(calc)
    E = atoms.get_potential_energy()

    return E









