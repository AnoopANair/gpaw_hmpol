import numpy as np
from numpy import sqrt, pi, exp, abs
from scipy.special import erf

import _gpaw
from gpaw import debug
from gpaw.utilities.tools import coordinates
from gpaw.utilities import is_contiguous


def Y_L(L, x, y, z, r2):
    if L == 0:
        return 0.28209479177387814
    elif L == 1:
        return 0.48860251190291992 * y
    elif L == 2:
        return 0.48860251190291992 * z
    elif L == 3:
        return 0.48860251190291992 * x
    elif L == 4:
        return 1.0925484305920792 * x * y
    elif L == 5:
        return 1.0925484305920792 * y * z
    elif L == 6:
        return 0.31539156525252005 * (3 * z * z - r2)
    elif L == 7:
        return 1.0925484305920792 * x * z
    elif L == 8:
        return 0.54627421529603959 * (x * x - y * y)
    # Not so machine generated code
    elif L == 9:
        return 0.5900435899266435 * (3 * x * x * y - y * y * y)
    elif L == 10:
        return 2.890611442640554 * x * y * z
    elif L == 11:
        return 0.4570457994644658 * (5 * y * z * z - y * r2)
    elif L == 12:
        return 0.3731763325901154 * (5 * z * z * z - 3 * z * r2)
    elif L == 13:
        return 0.4570457994644658 * (5 * x * z * z - x * r2)
    elif L == 14:
        return 1.445305721320277 * (x * x * z - y * y * z)
    elif L == 15:
        return 0.5900435899266435 * (x * x * x - 3 * x * y * y)
    elif L == 16:
        return 2.5033429417967046 * (x * x * x * y - x * y * y * y)
    elif L == 17:
        return 1.7701307697799307 * (3 * x * x * y * z - y * y * y * z)
    elif L == 18:
        return 0.9461746957575601 * (7 * x * y * z * z - x * y * r2)
    elif L == 19:
        return 0.6690465435572892 * (7 * y * z * z * z - 3 * y * z * r2)
    elif L == 20:
        return 0.10578554691520431 * (35 * z * z * z * z - 30 * z * z * r2 + 3 * r2 * r2)
    elif L == 21:
        return 0.6690465435572892 * (7 * x * z * z * z - 3 * x * z * r2)
    elif L == 22:
        return 0.47308734787878004 * (7 * x * x * z * z - 7 * y * y * z * z  
                                                        - x * x * r2 + y * y * r2)
    elif L == 23:
        return 1.7701307697799307 * (x * x * x * z - 3 * x * y * y * z)
    elif L == 24:
        return 0.6258357354491761 * (x * x * x * x + y * y * y * y - 6 * x * x * y * y)

def coeff_Y(L):
    if L == 0:
        return 0.28209479177387814
    elif L == 1:
        return 0.48860251190291992
    elif L == 2:
        return 0.48860251190291992
    elif L == 3:
        return 0.48860251190291992
    elif L == 4:
        return 1.0925484305920792 
    elif L == 5:
        return 1.0925484305920792 
    elif L == 6:
        return 0.31539156525252005
    elif L == 7:
        return 1.0925484305920792 
    elif L == 8:
        return 0.54627421529603959
    # Not so machine generated code
    elif L == 9:
        return 0.5900435899266435 
    elif L == 10:
        return 2.890611442640554
    elif L == 11:
        return 0.4570457994644658 
    elif L == 12:
        return 0.3731763325901154 
    elif L == 13:
        return 0.4570457994644658 
    elif L == 14:
        return 1.445305721320277
    elif L == 15:
        return 0.5900435899266435
    elif L == 16:
        return 2.5033429417967046
    elif L == 17:
        return 1.7701307697799307 
    elif L == 18:
        return 0.9461746957575601 
    elif L == 19:
        return 0.6690465435572892 
    elif L == 20:
        return 0.10578554691520431
    elif L == 21:
        return 0.6690465435572892 
    elif L == 22:
        return 0.47308734787878004                  
    elif L == 23:
        return 1.7701307697799307 
    elif L == 24:
        return 0.6258357354491761 


def coeff_G(L):
    if L == 0:  
        return sqrt(4) / pi
    elif L == 1:         
        return sqrt(5.333333333333333) / pi 
    elif L == 2:         
        return sqrt(5.333333333333333) / pi 
    elif L == 3:         
        return sqrt(5.333333333333333) / pi 
    elif L == 4:         
        return sqrt(4.2666666666666666) / pi 
    elif L == 5:         
        return sqrt(4.2666666666666666) / pi 
    elif L == 6:                
        return sqrt(0.35555555555555557) / pi 
    elif L == 7:    
        return sqrt(4.2666666666666666) / pi
    elif L == 8:    
        return sqrt(1.0666666666666667) / pi 

def gauss_L(a, L, x, y, z, r2, exp_ar2):
    if L == 0:
        return sqrt(a**3 * 4) / pi * exp_ar2
    elif L == 1:
        return sqrt(a**5 * 5.333333333333333) / pi * y * exp_ar2
    elif L == 2:
        return sqrt(a**5 * 5.333333333333333) / pi * z * exp_ar2
    elif L == 3:
        return sqrt(a**5 * 5.333333333333333) / pi * x * exp_ar2
    elif L == 4:
        return sqrt(a**7 * 4.2666666666666666) / pi * x * y * exp_ar2
    elif L == 5:
        return sqrt(a**7 * 4.2666666666666666) / pi * y * z * exp_ar2
    elif L == 6:
        return sqrt(
            a**7 * 0.35555555555555557) / pi * (3 * z * z - r2) * exp_ar2
    elif L == 7:
        return sqrt(a**7 * 4.2666666666666666) / pi * x * z * exp_ar2
    elif L == 8:
        return sqrt(a**7 * 1.0666666666666667) / pi * (x * x - y * y) * exp_ar2
    # beginning of not so machine generated code:
    elif L == 9:                       
        return 0.10145415536022434 * sqrt(a**9) * exp_ar2 * (3 * x * x * y - y * y * y)
    elif L == 10:                      
        return 0.497021825835201   * sqrt(a**9) * exp_ar2 * x * y * z     
    elif L == 11:                      
        return 0.07858605082273777 * sqrt(a**9) * exp_ar2 * (5 * y * z * z - y * r2)
    elif L == 12:                      
        return 0.0641652418053779  * sqrt(a**9) * exp_ar2 * (5 * z * z * z - 3 * z * r2)
    elif L == 13:                      
        return 0.07858605082273777 * sqrt(a**9) * exp_ar2 * (5 * x * z * z - x * r2)
    elif L == 14:                      
        return 0.2485109129176005  * sqrt(a**9) * exp_ar2 * (x * x * z - y * y * z)
    elif L == 15:                      
        return 0.10145415536022434 * sqrt(a**9) * exp_ar2 * (x * x * x - 3 * x * y * y)
    elif L == 16:
        return 0.09565189497969086  * sqrt(a**11) * exp_ar2 * (x * x * x * y - x * y * y * y)
    elif L == 17:
        return 0.06763610357348289  * sqrt(a**11) * exp_ar2 * (3 * x * x * y * z - y * y * y * z)
    elif L == 18:
        return 0.03615301807833281  * sqrt(a**11) * exp_ar2 * (7 * x * y * z * z - x * y * r2)
    elif L == 19:
        return 0.02556404424354897  * sqrt(a**11) * exp_ar2 * (7 * y * z * z * z - 3 * y * z * r2)
    elif L == 20:
        return 0.004042030300746549 * sqrt(a**11) * exp_ar2 * (35 * z * z * z * z 
                                                             - 30 * z * z * r2 + 3 * r2 * r2)
    elif L == 21:
        return 0.02556404424354897  * sqrt(a**11) * exp_ar2 * (7 * x * z * z * z - 3 * x * z * r2)
    elif L == 22:
        return 0.018076509039166404 * sqrt(a**11) * exp_ar2 * (7 * x * x * z * z - 7 * y * y * z * z
                                                        - x * x * r2 + y * y * r2)
    elif L == 23:
        return 0.06763610357348289  * sqrt(a**11) * exp_ar2 * (x * x * x * z - 3 * x * y * y * z)
    elif L == 24:
        return 0.023912973744922714 * sqrt(a**11) * exp_ar2 * (x * x * x * x + y * y * y * y 
                                                             - 6 * x * x * y * y)



def gausspot_L(a, L, x, y, z, r, r2, erf_sar, exp_ar2):
    if L == 0:
        return 2.0 * 1.7724538509055159 * erf_sar / r
    elif L == 1:
        return 1.1547005383792515 * (1.7724538509055159 * erf_sar -
                                     2 * sqrt(a) * r * exp_ar2) / r / r2 * y
    elif L == 2:
        return 1.1547005383792515 * (1.7724538509055159 * erf_sar -
                                     2 * sqrt(a) * r * exp_ar2) / r / r2 * z
    elif L == 3:
        return 1.1547005383792515 * (1.7724538509055159 * erf_sar -
                                     2 * sqrt(a) * r * exp_ar2) / r / r2 * x
    elif L == 4:
        return 0.5163977794943222 * (
            5.3173615527165481 * erf_sar -
            (6 + 4 *
             (sqrt(a) * r)**2) * sqrt(a) * r * exp_ar2) / r / r2**2 * x * y
    elif L == 5:
        return 0.5163977794943222 * (
            5.3173615527165481 * erf_sar -
            (6 + 4 *
             (sqrt(a) * r)**2) * sqrt(a) * r * exp_ar2) / r / r2**2 * y * z
    elif L == 6:
        return 0.14907119849998599 * (5.3173615527165481 * erf_sar -
                                      (6 + 4 *
                                       (sqrt(a) * r)**2) * sqrt(a) * r *
                                      exp_ar2) / r / r2**2 * (3 * z * z - r2)
    elif L == 7:
        return 0.5163977794943222 * (
            5.3173615527165481 * erf_sar -
            (6 + 4 *
             (sqrt(a) * r)**2) * sqrt(a) * r * exp_ar2) / r / r2**2 * x * z
    elif L == 8:
        return 0.2581988897471611 * (5.3173615527165481 * erf_sar -
                                     (6 + 4 * (sqrt(a) * r)**2) * sqrt(a) * r *
                                     exp_ar2) / r / r2**2 * (x * x - y * y)


# end of computer generated code


class Gaussian:
    r"""Class offering several utilities related to the generalized gaussians.

    Generalized gaussians are defined by::

                       _____                           2
                      /  1       l!         l+3/2  -a r   l  m
       g (x,y,z) =   / ----- --------- (4 a)      e      r  Y (x,y,z),
        L          \/  4 pi  (2l + 1)!                       l

    where a is the inverse width of the gaussian, and Y_l^m is a real
    spherical harmonic.
    The gaussians are centered in the middle of input grid-descriptor."""
    def __init__(self, gd, a=19., center=None):
        self.gd = gd
        self.xyz, self.r2 = coordinates(gd, center)
        self.r = np.sqrt(self.r2)
        self.set_width(a, center)
        self.exp_ar2 = exp(-self.a * self.r2)
        self.erf_sar = erf(sqrt(self.a) * self.r)

    def set_width(self, a, center):
        """Set exponent of exp-function to -a on the boundary."""
        if center is None:
            self.a = 4 * a * (self.gd.icell_cv**2).sum(1).max()
        else:
            cell_center = self.gd.cell_cv.sum(1) / 2
            r_min = (cell_center - np.abs(center - cell_center)).min()
            self.a = a / r_min**2

    def get_gauss(self, L):
        a = self.a
        x, y, z = tuple(self.xyz)
        r2 = self.r2
        exp_ar2 = self.exp_ar2
        return gauss_L(a, L, x, y, z, r2, exp_ar2)

    def get_gauss_pot(self, L):
        a = self.a
        x, y, z = tuple(self.xyz)
        r2 = self.r2
        r = self.r
        erf_sar = self.erf_sar
        exp_ar2 = self.exp_ar2
        return gausspot_L(a, L, x, y, z, r, r2, erf_sar, exp_ar2)

    def get_moment(self, n, L):
        r2 = self.r2
        x, y, z = tuple(self.xyz)
        return self.gd.integrate(n * Y_L(L, x, y, z, r2))

    def remove_moment(self, n, L, q=None):
        # Determine multipole moment
        if q is None:
            q = self.get_moment(n, L)

        # Don't do anything if moment is less than the tolerance
        if abs(q) < 1e-7:
            return 0.

        # Remove moment from input density
        n -= q * self.get_gauss(L)

        # Return correction
        return q * self.get_gauss_pot(L)


def gaussian_wave(r_vG, r0_v, sigma, k_v=None, A=None, dtype=float,
                  out_G=None):
    r"""Generates function values for atom-centered Gaussian waves.

    ::

                         _ _
        _            / -|r-r0|^2 \           _ _
      f(r) = A * exp( ----------- ) * exp( i k.r )
                     \ 2 sigma^2 /

    If the parameter A is not specified, the Gaussian wave is normalized::

                                                  oo
           /    ____        \ -3/2               /       _  2  2
      A = (    /    '        )        =>    4 pi | dr |f(r)|  r  = 1
           \ \/  pi   sigma /                    /
                                                   0

    Parameters:

    r_vG: ndarray
        Set of coordinates defining the grid positions.
    r0_v: ndarray
        Set of coordinates defining the center of the Gaussian envelope.
    sigma: float
        Specifies the spatial width of the Gaussian envelope.
    k_v: ndarray or None
        Set of reciprocal lattice coordinates defining the wave vector.
        An argument of None is interpreted as the gamma point i.e. k_v=0.
    A: float, complex or None
        Specifies the amplitude of the Gaussian wave. Normalizes if None.
    dtype: type, defaults to float
        Specifies the output data type. Only returns the real-part if float.
    out_G: ndarray or None
        Optional pre-allocated buffer to fill in values. Allocates if None.

    """
    if k_v is None:
        k_v = np.zeros(r0_v.shape)

    if A is None:
        # 4*pi*int(exp(-r^2/(2*sigma^2))^2 * r^2, r=0...infinity)
        # = sigma^3*pi^(3/2) = 1/A^2 -> A = (sqrt(Pi)*sigma)^(-3/2)
        A = 1 / (sigma * np.pi**0.5)**1.5

    if debug:
        assert is_contiguous(r_vG, float)
        assert is_contiguous(r0_v, float)
        assert is_contiguous(k_v, float)
        assert r_vG.ndim >= 2 and r_vG.shape[0] > 0
        assert r0_v.ndim == 1 and r0_v.shape[0] > 0
        assert k_v.ndim == 1 and k_v.shape[0] > 0
        assert (r_vG.shape[0], ) == r0_v.shape == k_v.shape
        assert sigma > 0

    if out_G is None:
        out_G = np.empty(r_vG.shape[1:], dtype=dtype)
    elif debug:
        assert is_contiguous(out_G)
        assert out_G.shape == r_vG.shape[1:]

    # slice_v2vG = [slice(None)] + [np.newaxis]*3
    # gw = lambda r_vG, r0_v, sigma, k_v, A=1/(sigma*np.pi**0.5)**1.5: \
    #    * np.exp(-np.sum((r_vG-r0_v[slice_v2vG])**2, axis=0)/(2*sigma**2)) \
    #    * np.exp(1j*np.sum(np.r_vG*k_v[slice_v2vG], axis=0)) * A
    _gpaw.utilities_gaussian_wave(A, r_vG, r0_v, sigma, k_v, out_G)
    return out_G
